﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour {

	GameObject obj;
	float lastMove;

	void Awake()
	{
		lastMove = 0;
	}
	public void Update()
	{
		if (Time.time > lastMove)
		{

			if (Input.GetKey(KeyCode.UpArrow))
	        {
	        	Vector3 pos = transform.localPosition;
	        	pos.y += 1;
	        	transform.localPosition = pos;
	        	updateTime();
	        }
	        else if (Input.GetKey("down"))
	        {
	        	Vector3 pos = transform.localPosition;
	        	pos.y -= 1;
	        	transform.localPosition = pos;
	        	updateTime();
	        }
	        else if (Input.GetKey("left"))
	        {
	        	Vector3 pos = transform.localPosition;
	        	pos.x -= 1;
	        	transform.localPosition = pos;
	        	updateTime();
	        }
	        else if (Input.GetKey("right"))
	        {
	        	Vector3 pos = transform.localPosition;
	        	pos.x += 1;
	        	transform.localPosition = pos;
	        	updateTime();
	        }
		}
	}

	private void updateTime()
	{
		lastMove = Time.time + 0.15f;
	}
}
