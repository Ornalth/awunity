﻿using UnityEngine;
using System.Collections;

public class CloseChildrenScript : MonoBehaviour {

	// Use this for initialization
	//void Start () {
		/*Renderer[] re = GetComponentsInChildren<Renderer> ();
		foreach (Renderer r in re)
		{
			r.enabled = false;
		}*/
	

		/*MainBoardVC[] a = GetComponentsInChildren<MainBoardVC> ();
		foreach (MainBoardVC vc in a)
		{
			vc.enabled = false;
		}*/
	//}
	
	public void closeChildren()
	{
		foreach( Transform child in transform )
		{
			child.gameObject.SetActive( false );
		} 
	}

}
