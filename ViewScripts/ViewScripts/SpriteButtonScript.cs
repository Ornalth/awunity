﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class SpriteButtonScript : MonoBehaviour {

	List<ISpriteButtonListener> listeners = new List<ISpriteButtonListener>();


	public void addListener(ISpriteButtonListener listener)
	{
		listeners.Add(listener);
	}

	public void removeListener(ISpriteButtonListener listener)
	{
		listeners.Remove(listener);
	}

	void OnMouseDown () {
		
		foreach (ISpriteButtonListener listener in listeners)
		{
			listener.buttonClicked(this);
		}
	}
}
