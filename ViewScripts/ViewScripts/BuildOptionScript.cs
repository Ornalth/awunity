﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class BuildOptionScript : MonoBehaviour {

	public interface BuildOptionListener 
	{
		void buildOptionSelected(BuildOptionScript scr);
	}



	BuildOptionDetail details;

	Text nameText;
	Text costText;
	Image unitImage;
	Button myself;
	List<BuildOptionListener> listeners = new List<BuildOptionListener>();



	void Awake()
	{
		Text[] texts = GetComponentsInChildren<Text>();
		foreach (Text text in texts)
		{
			if (text.name.Equals("Name"))
			{
				nameText = text;
			}
			else if (text.name.Equals("Cost"))
			{
				costText = text;
			}
		}

		Image[] images = GetComponentsInChildren<Image>();
		foreach (Image image in images)
		{
			if (image.name.Equals("Image"))
			{
				unitImage = image;
			}
		}

		myself = GetComponent<Button>();
 
        myself.onClick.AddListener(buttonClicked);
	}

	public void subscribe(BuildOptionListener scr)
	{
		listeners.Add(scr);
	}

	public void buttonClicked()
	{
		foreach (BuildOptionListener listener in listeners)
		{
			listener.buildOptionSelected(this);
		}
	}
	
	public void setDetails(BuildOptionDetail details)
	{
		this.details = details;
		nameText.text = details.unit.getName();
		costText.text = "" + UnitFactory.getCost(details.unit);
		unitImage.sprite = ImageLibrary.getImage(details.unit.getDrawableName());
		myself.interactable = details.canBuild;

	}

	public BuildOptionDetail getDetails()
	{
		return details;

	}
}
