using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;

//using Holoville.HOTween; 
//using Holoville.HOTween.Plugins;

public class MainBoardVC: MonoBehaviour, Observer, TileViewScript.TileObserver,
					BuildingViewScript.BuildingViewListener,  GenericStatusDetailsViewScript.GenericStatusDetailsListener,
					DetailsViewScript.DetailsViewScriptListener 

{

	public enum TileMode
	{
		ATTACK, BUILD, MOVE, NONE, CONFIRM_ATTACK, FINISHED
	}

	private TileMode mode = TileMode.NONE;
	List<GameObject> unitObjs = new List<GameObject>();
	List<GameObject> buildingObjs = new List<GameObject>();

	List<List<GameObject>> gameMap = new List<List<GameObject>>(); //TILES
	List<List<TileViewScript>> tileMapScripts = new List<List<TileViewScript>>();
	
	HashSet<Position> actedSet = new HashSet<Position>();
	
	Board board;
	private List<Position> selectArray = new List<Position>();
	Tile origin;
	Building selectedBuilding;

	int centeredLocation;
	Position confirmPosition;

	BuildingViewScript buildScript;
	GameObject buildingViewObject;

	GenericStatusDetailsViewScript statusScript;
	GameObject statusViewObject;

	DetailsViewScript detailsViewScript;
	GameObject detailsViewObject;

	GameObject UIHolderObject;

	GameObject damageHintObject;
	DamageHintView damageHintViewScript;


	void Awake()
	{
		UIHolderObject = GameObject.FindWithTag("UIHolder");
		buildingViewObject = Instantiate (Resources.Load("Prefabs/BuildingViewCanvas")) as GameObject;
		buildScript = buildingViewObject.GetComponent<BuildingViewScript>();
		buildScript.subscribe(this);
		ViewHelper.hide(buildingViewObject);
		buildingViewObject.transform.parent = UIHolderObject.transform;

		statusViewObject = Instantiate (Resources.Load("Prefabs/GenericDetailsCanvas")) as GameObject;
		statusScript = statusViewObject.GetComponent<GenericStatusDetailsViewScript>();
		statusScript.subscribe(this);
		//ViewHelper.hide(statusViewObject);
		statusViewObject.transform.parent = UIHolderObject.transform;

		detailsViewObject = Instantiate (Resources.Load("Prefabs/InfoViewCanvas")) as GameObject;
		detailsViewScript = detailsViewObject.GetComponent<DetailsViewScript>();
		detailsViewScript.subscribe(this);
		//ViewHelper.hide(detailsViewObject);
		detailsViewObject.transform.parent = UIHolderObject.transform;

		damageHintObject = Instantiate (Resources.Load("Prefabs/DamageHintView")) as GameObject;
		damageHintViewScript = damageHintObject.GetComponent<DamageHintView>();
		//damageHintViewScript.subscribe(this);
		ViewHelper.hide(damageHintObject);
		damageHintObject.transform.parent = gameObject.transform;

		board = Board.getInstance();
		board.subscribe(this);
		for (int row = 0; row < Constants.height; row++)
		{
			List<TileViewScript> tileRowScripts = new List<TileViewScript>();
			List<GameObject> rowMap = new List<GameObject>();
			for (int col = 0; col < Constants.width; col++)
			{
				Position pos = new Position (row, col);
				Tile tile = board.getTile(pos);
				GameObject tileObj = Instantiate (Resources.Load("Prefabs/Tile")) as GameObject;
				tileObj.name = pos.ToString();
				tileObj.transform.localPosition = new Vector3(col - Constants.width/2,row - Constants.height/2,1);
				tileObj.transform.parent = this.transform;
				rowMap.Add(tileObj);
				TileViewScript script = tileObj.GetComponent<TileViewScript>();
				script.setTile(tile);
				script.setPosition(pos);
				script.subscribe(this);
				tileRowScripts.Add(script);

			}
			gameMap.Add(rowMap);
			tileMapScripts.Add(tileRowScripts);
		}
		
		updatePlayer();
	}


	public void tileEntered(TileViewScript t)
	{
		if (mode == TileMode.ATTACK)
		{
			Position pos = t.getPosition();
			Tile tile = board.getTile(pos);
			//Debug.Log("HELLO WORLD");
			if (selectArray.Contains(pos))
			{
				//??!!!?! 
				Debug.Log("!!!");
				int estDmg = CombatCoordinator.getDamage(origin.getUnit(), detailsViewScript.getWeaponSelection(), tile.getUnit());
				showDamageIndicator(pos, estDmg);
			}
		}
	}


	private void showDamageIndicator(Position pos, int damage)
	{
		Vector3 loc = damageHintObject.transform.localPosition;
		loc.x = pos.col - 7 + 0.5f;
		loc.y = pos.row - 7 + 0.5f;
		damageHintObject.transform.localPosition = loc;
		damageHintViewScript.setDamage(damage);
		ViewHelper.show(damageHintObject);
	}

	private void hideDamageIndicator()
	{
		ViewHelper.hide(damageHintObject);
	}


	//@Override
	public void tileClicked(TileViewScript t)
	{
		if (!board.isHumanPlayerTurn())
		{
			return;
		}
		//Debug.Log("CLICKED POS " + t.getPosition().ToString());
		Tile tile = board.getTile(t.getPosition());
		
		
		if (mode == TileMode.ATTACK)
		{
			if (selectArray.Contains(t.getPosition()))
			{
				bool isPrimary = detailsViewScript.getWeaponSelection();
				/*if (origin.getUnit().getWeapon(isPrimary).radius == 0)
				{
					if (infoView.getSelectedTarget() != null && infoView.getSelectedTarget().equals(t.getPosition()))
					{
						board.attack(origin, t, isPrimary);
						// board.finishedTile(origin);
						setMode(TileMode.NONE);
					}
					else
					{
						Position p = t.getPosition();
						boardView.setConfirmAttack(p);
						infoView.selectTarget(p);
						// board.finishedTile(origin);
						//setMode(TileMode.NONE);
					}
			
					
				}
				else
				{
					board.attack(origin, t, isPrimary);
					// board.finishedTile(origin);
					setMode(TileMode.NONE);
				}*/
				board.attack(origin, tile, isPrimary);
				// board.finishedTile(origin);
				showTile(origin);
				setMode(TileMode.NONE);
				// origin = t;
			}
			else
			{
				board.finishedTile(origin);
				setMode(TileMode.NONE);
			}
		}
		else if (mode == TileMode.MOVE)
		{

			if (selectArray.Contains(t.getPosition()))
			{

				board.move(origin, tile);
				// setMode(TileMode.NONE);
				removeSelected(selectArray);
				origin = tile;
				showTile(origin);

				if (tile.getUnit().canAttack())
				{
					setMode(TileMode.ATTACK);
					showAttackOptions(tile, tile.getUnit().getFirstWeaponChoice());
				
				}
				else
				{
					setMode(TileMode.NONE);
				}
			}
			else
			{
				setMode(TileMode.NONE);
			}
		}
		else if (mode == TileMode.BUILD || mode == TileMode.NONE)
		{
			Unit unit = tile.getUnit();
			origin = tile;
			if (unit != null)
			{
				if (unit.getTeam() == board.getCurrentTeam() && unit.canMove())
				{
					setMode(TileMode.MOVE);
					showMoveOptions(tile);
				}
			}
			else if (tile.isBuilding()
					&& ((Building) tile).getTeam() == board.getCurrentTeam())
			{
				Building build = (Building) tile;
				if (build.canGenerate())
				{
					setMode(TileMode.BUILD);

					List<Unit> units = build.getGenerateOptions();
					showBuildGenerationOptions((Building) tile, units);
				}
				detailsViewScript.showTileDetails(tile);
			}
			else
			{
				setMode(TileMode.NONE);
				//swapToInfoView();
			}
			showTile(tile);
		}



	}

	public void setConfirmAttack(Position p)
	{
		if (confirmPosition != null)
		{
			TileViewScript tile = tileMapScripts[confirmPosition.row][confirmPosition.col];
			tile.setHighlight(TileViewScript.State.ATTACK_TARGET);
		}
		
		confirmPosition = p;
		TileViewScript tileView = tileMapScripts[p.row][p.col];
		tileView.setHighlight(TileViewScript.State.CONFIRM_ATTACK_TARGET);
	}

	public void showMoveOptions(Tile t)
	{
		List<Position> pos = board.getPossibleMoves(t);
		selectArray = pos;
		setSelected(selectArray, mode);
	}

	private void showBuildGenerationOptions(Building build,
			List<Unit> units)
	{
		int team = build.getTeam();
		int gold = board.getGold(team);

		buildScript.setData(build, units, gold, team);
		ViewHelper.show(buildingViewObject);
		selectArray = new List<Position>();
		selectArray.Add(build.getPosition());
		selectedBuilding = build;
		setSelected(selectArray, mode);
	}

	public void showAttackOptions(Tile t, bool isPrimary)
	{
		List<Position> positions = board.getTargets(t, isPrimary);
		selectArray = positions;
		setSelected(selectArray, mode);
		
		detailsViewScript.setWeaponSelection(isPrimary);
		
		if (t.getUnit().getWeapon(isPrimary).radius == 0)
		{
			if(selectArray.Count > 0)
			{
				Position pos = positions[0];
			//	boardView.setConfirmAttack(pos);
			//	infoView.showTargetList(t, positions);
			}
		
			//infoView.showEstimateDamage(estimate);
		}
	
	}

	private void setMode(TileMode m)
	{
		this.mode = m;
		switch (mode)
		{
			case TileMode.ATTACK:
			{
				break;
			}
			case TileMode.BUILD:
			{
				//clearTile();
				origin = null;
				removeSelected(selectArray);
				selectArray.Clear();
				break;
			}
			case TileMode.MOVE:
			{
				break;
			}
			case TileMode.NONE:
			{
				//clearTile();
				origin = null;
				removeSelected(selectArray);
				selectArray.Clear();
				hideDamageIndicator();
				break;
			}
			default:
				break;

		}

	}

	/**
	 * ehh?
	 */
	public void showTile(Tile t)
	{
		//swapToInfoView();
		detailsViewScript.showTileDetails(t);
	}

	public void clearTile()
	{
		//swapToInfoView();
		detailsViewScript.clearTileDetails();
	}



	public void setSelected(List<Position> selectArray,
			TileMode mode)
	{
		foreach (Position p in selectArray)
		{
			TileViewScript tileView = tileMapScripts[p.row][p.col];
			switch (mode)
			{
				case TileMode.ATTACK:
					tileView.setHighlight(TileViewScript.State.ATTACK_TARGET);
					break;
				case TileMode.BUILD:
					tileView.setHighlight(TileViewScript.State.BUILD);
					break;
				case TileMode.MOVE:
					tileView.setHighlight(TileViewScript.State.MOVE_TARGET);
					break;
				case TileMode.NONE:
					tileView.setHighlight(TileViewScript.State.NONE);
					break;
				default:
					break;
			}
		}
	}

	public void removeSelected(List<Position> selectArray)
	{
		foreach (Position p in selectArray)
		{
			TileViewScript tileView = tileMapScripts[p.row][p.col];
			tileView.setHighlight(TileViewScript.State.NONE);
		}
		confirmPosition = null;
	}


	//Board Observer
	public void update(object obj, string reason)
	{

	}

	public void update(List<Position> positions)
	{
		updateBoard(positions);
	}

	public void update(Position position)
	{
		updateBoard(new List<Position>(){position});
	}

	public void update(Player player)
	{
		
	}

	public void gameFinished(int winner)
	{

	}

	public void newTurn()
	{
		renewActed();
		setMode(TileMode.NONE);
		updatePlayer();
	}

	public void updatePlayer()
	{
		statusScript.update(board);
	}

	public void renewActed()
	{
		foreach (Position p in actedSet)
		{
			TileViewScript tileView = tileMapScripts[p.row][p.col];
			tileView.setFinished(false);
		}
		actedSet = new HashSet<Position>();
	}



	void updateBoard(List<Position> arPos)
	{
		foreach (Position p in arPos)
		{
			TileViewScript tileView = tileMapScripts[p.row][p.col];
			Tile tile = board.getTile(p);

			tileView.setTile(tile);

			if (tile.hasUnit() && tile.getUnit().getTeam() == board.getCurrentTeam() && (!tile.getUnit().canMove() && !tile.getUnit().canAttack()))
			{
				tileView.setFinished(true);
				actedSet.Add(p);
			}
		}
		updatePlayer();
	}

	//Building View Script observer
	public void buildOptionSelected(BuildingViewScript scr, BuildOptionDetail details)
	{
		ViewHelper.hide(buildingViewObject);
		board.purchase(details.unit, selectedBuilding);
		setMode(TileMode.NONE);
		showTile(selectedBuilding);

	}	

	public void buildOptionCancelled(BuildingViewScript scr)
	{
		ViewHelper.hide(buildingViewObject);
		setMode(TileMode.NONE);
	}


	// GenericStatusDetailsViewScript.GenericStatusDetailsListener 

	public void endTurnRequest(GenericStatusDetailsViewScript scr)
	{
		board.endTurn();
	}

	public void regularAbilityRequest(GenericStatusDetailsViewScript scr)
	{
		board.activateRegularAbility();
	}

	public void superAbilityRequest(GenericStatusDetailsViewScript scr)
	{
		board.activateSuperAbility();
	}

	public void undoMoveRequest(GenericStatusDetailsViewScript scr)
	{
		board.undoMove();
		setMode(TileMode.NONE);
	}

	//DetailsViewScript.DetailsViewScriptListener
	public void captureBuildingRequest(DetailsViewScript scr)
	{
		if (mode == TileMode.MOVE || mode == TileMode.ATTACK)
		{
			// Returns if successful cap!.
			if (board.cap(origin))
			{
				setMode(TileMode.NONE);
			}
			else
			{
				Debug.Log("FAILED CAP");
				setMode(TileMode.NONE);
			}
		}
	}
	
	public void attackRequest(DetailsViewScript scr)
	{
		if (origin != null 
			&& mode == TileMode.MOVE
			&& origin.hasUnit() 
			&& origin.getUnit().canAttack())
		{
			removeSelected(selectArray);
			setMode(TileMode.ATTACK);
			showAttackOptions(origin, origin.getUnit().getFirstWeaponChoice());
		}	
	}


	public void weaponSelected(bool isPrimary)
	{
		if (mode == TileMode.ATTACK)
		{
			//Update  attack selected tiles.
		}
	}
}