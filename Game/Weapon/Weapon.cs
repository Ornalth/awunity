﻿
public class Weapon
{
	private bool InstanceFieldsInitialized = false;

	private void InitializeInstanceFields()
	{
		ammo = maxAmmo;
	}

	public string name = "Nothing";
	public int radius = 0;
	public int baseDamage = 0;
	public DamageType type = DamageType.EXPLOSIVE;
	public int minRange = 0;
	public int maxRange = 0;
	public int maxAmmo = 0;
	public int ammo;
	public bool retal = false;

	public static Weapon DEFAULT_WEAPON = new Weapon();

	public bool isDefaultWeapon()
	{
		return this.Equals(DEFAULT_WEAPON);
	}
	public Weapon(Weapon w)
	{
		if (!InstanceFieldsInitialized)
		{
			InitializeInstanceFields();
			InstanceFieldsInitialized = true;
		}
		retal = w.retal;
		name = w.name;
		radius = w.radius;
		baseDamage = w.baseDamage;
		type = w.type;
		minRange = w.minRange;
		maxRange = w.maxRange;
		maxAmmo = w.maxAmmo;
		ammo = maxAmmo;

	}

	public Weapon()
	{
		if (!InstanceFieldsInitialized)
		{
			InitializeInstanceFields();
			InstanceFieldsInitialized = true;
		}
	}

	public virtual void removeAmmo()
	{
		if (ammo != Constants.INFINITE)
		{
			ammo--;
		}
	}

	public virtual void resupply()
	{
		ammo = maxAmmo;
	}

	public virtual bool hasAmmo()
	{
		return ammo > 0;
	}

	public virtual bool canRetal()
	{
		return retal;
	}

	public virtual bool hasInfiniteAmmo()
	{
		if (ammo >= Constants.INFINITE)
		{
			return true;
		}
		return false;
	}

	public override bool Equals(object o)
	{


		if (o == null)
		{
			return false;
		}

		if (o is Weapon)
		{
			return name.Equals(((Weapon)o).name);
		}
		return false;
	}

}
