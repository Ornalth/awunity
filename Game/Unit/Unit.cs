using System;


//TODO MOVE ALL JUST TO UNIT, USE ENUM FOR TYPE AND MOVEMENT!
//TODO MOVE CAPTURE TO GENERIC BOOLEAN SO ANY UNIT CAN POSSILBY CAP. (NOT ONLY FOOT)
public class Unit : IDrawable
{
	protected internal int health;
	protected internal int maxHealth;
	protected internal int team;
	protected internal int moves;
	protected internal bool cMoveAndAttack;
	protected internal Weapon primary = Weapon.DEFAULT_WEAPON;
	protected internal Weapon secondary = Weapon.DEFAULT_WEAPON;
	protected internal string name;
	protected internal int id;

	protected internal bool cCapture = false;
	protected internal double capSpeed = 1;

	protected string drawableName;
	protected MovementType moveType;
	
	// Inside game info no need to copy.
	public int row;
	public int col;
	protected internal bool hMoved;
	protected internal bool hAttacked;

	protected internal int kills = 0;

	protected internal int maxGas = 0;
	protected internal int gas = 0;
	protected internal int gasUpkeep = 0;
	protected internal bool dieOnGasless = false;
	protected internal DamageType armor;
	protected UnitRank rank;

	public Unit()
	{

	}

	public Unit(Unit unit)
	{
		rank = UnitRank.RECRUIT;
		armor = unit.armor;
		health = unit.getHealth();
		maxHealth = unit.getMaxHealth();
		team = unit.getTeam();
		moves = unit.moves;

		cMoveAndAttack = unit.cMoveAndAttack;

		primary = new Weapon(unit.primary);
		secondary = new Weapon(unit.secondary);

		cCapture = unit.cCapture;
		capSpeed = unit.capSpeed;

		id = unit.id;
		name = unit.name;

		maxGas = unit.maxGas;
		gas = maxGas;
		gasUpkeep = unit.gasUpkeep;
		dieOnGasless = unit.dieOnGasless;

		row = unit.row;
		col = unit.col;
		kills = 0;
		hMoved = true;
		hAttacked = true;
		moveType = unit.moveType;
		drawableName = unit.drawableName;
	}

	public Unit deepCopy()
	{
		return new Unit(this);
	}

	public virtual void repair()
	{
		health += (int)(Constants.UNIT_REGEN_ON_BUIDING_PERCENTAGE * maxHealth);
		if (health > maxHealth)
		{
			health = maxHealth;
		}
	}

	public virtual bool move()
	{
		if (hMoved)
		{
			MyLogger.log("ERROR IN UNIT: CANNOT MOVE");
			return false;
		}
		hMoved = true;
		return true;
	}

	public void undoMove()
	{
		hMoved = false;
	}

	/// 
	/// <param name="tile"> </param>
	/// <param name="isPrimary"> </param>
	/// <returns> baseDamage. & radius </returns>
	public virtual AttackObject attack(Position selfPos, Tile tile, bool isPrimary)
	{
		hAttacked = true;
		AttackObject atk = new AttackObject();
		Weapon weapon = isPrimary ? primary : secondary;
		if (weapon.ammo <= 0)
		{
			atk.error = ("NO AMMO");
			return atk;
		}
		else if (weapon.radius == 0)
		{
			if (tile.getUnit() == null)
			{
				atk.error = ("MUST TARGET 0 radius");
				return atk;
			}
		}
		else
		{

			int distance = Position.getDistance(selfPos, tile.getPosition());
			if (distance > weapon.maxRange || distance < weapon.minRange)
			{
				atk.error = ("DISTANCE ERROR");
				return atk;
			}
		}
		weapon.removeAmmo();
		atk.damage = CombatCoordinator.getDamage(this, isPrimary, tile.getUnit());
		atk.type = weapon.type;
		atk.radius = weapon.radius;
		return atk;

	}


	public MovementType getMovementType()
	{
		return moveType;
	}

	public void setMovementType(MovementType moveType)
	{
		this.moveType = moveType;
	}

	public UnitRank getRank()
	{
		return rank;
	}
	// TODO
	// max gas
	// max ammo
	// etc.
	public virtual void resupply()
	{
		gas = maxGas;
		primary.resupply();
		secondary.resupply();
	}

	public void checkPromotions()
	{
		UnitRank newRank = UnitRank.getRank(kills);
		this.rank = newRank;
	}

	public virtual void takeDamage(AttackObject obj, Unit source)
	{
		if (obj.damage > 0)
		{
			int dmg = obj.damage;
			health -= dmg;

			MyLogger.log(this.getName() + " took " + dmg + " from " + (source != null ? source.getName() : "idk"));
		}

	}

	public virtual void addKill(Unit unit)
	{
		kills++;

	}

	// TODO
	public virtual AttackObject retaliate(Tile atker)
	{
		int distance = Position.getDistance(getPosition(), atker.getPosition());
		AttackObject atk = new AttackObject();

		if (primary.ammo > 0 && primary.maxRange >= distance && primary.minRange <= distance && primary.radius == 0)
		{
			primary.removeAmmo();
			atk.damage = CombatCoordinator.getDamage(this, true, atker.getUnit());
			atk.type = primary.type;
			atk.radius = primary.radius;
		}
		else if (secondary.ammo > 0 && secondary.maxRange >= distance && secondary.minRange <= distance && secondary.radius == 0)
		{
			secondary.removeAmmo();
			atk.damage = CombatCoordinator.getDamage(this, false, atker.getUnit());
			atk.type = secondary.type;
			atk.radius = secondary.radius;
		}
		return atk;
	}

	/// <summary>
	/// getts/setts
	/// </summary>
	public string getName()
	{
		return name;
		
	}

	public void setName(string s)
	{
		this.name = s;
	}

	public virtual int getMaxHealth()
	{
		return maxHealth;
	}

	public void setMaxHealth(int value)
	{
		maxHealth = value;
		health = maxHealth;
	}

	public virtual void setMaxMoves(int value)
	{
		moves = value;
	}

	public void setId(int value)
	{
		id = value;
	}

	public int getId()
	{
		return id;
	}

	public void setMoves(int moves)
	{
		this.moves = moves;
	}

	public int getMoves()
	{
		return moves;
	}


	public virtual bool canMoveAndAttack()
	{
		return cMoveAndAttack;
	}

	public void setCanMoveAndAttack(bool moveAtk)
	{
		this.cMoveAndAttack = moveAtk;
	}

	public virtual bool hasMoved()
	{
		return hMoved;
	}

	public void setHasMoved(bool b)
	{
		this.hMoved = b;
	}

	public virtual bool hasAttacked()
	{
		return hAttacked;
	}

	public void setHasAttacked(bool b)
	{
		this.hAttacked = b;
	}

	public int getKills()
	{
			return kills;
	}

	public void setKills(int value)
	{
		this.kills = value;
	}

	public Weapon getPrimaryWeapon()
	{
		return primary;
	}

	public void setPrimaryWeapon(Weapon value)
	{
		primary = value;
	}

	public Weapon getSecondaryWeapon()
	{
		return secondary;
	}

	public void setSecondaryWeapon(Weapon value)
	{
		secondary = value;
	}

	public int getHealth()
	{
		return health;
	}

	public void setHealth(int value)
	{
		this.health = value;
	}

	public void setTeam(int value)
	{
		this.team = value;
	}

	public int getTeam()
	{
		return team;
	}

	public bool isDead()
	{
		if (gas < 0 && dieOnGasless)
		{
			return true;
		}
		return health <= 0;
	}

	public virtual bool canMove()
	{
		return !hMoved && !hAttacked;
	}

	public virtual bool canAttack()
	{
		if (!hAttacked)
		{
			if (!hMoved || cMoveAndAttack)
			{
				return primary.hasAmmo() || secondary.hasAmmo();
			}
		}
		return false;
	}

	public void setDrawableName(string name)
	{
		drawableName = name;
	}

	public string getDrawableName()
	{
		return drawableName + getTeam();
		//UnitFactory.getDrawableName(this);
	}


	public virtual bool usesGas()
	{
		return maxGas > 0;
	}

	public int getGasUpkeep()
	{
		return gasUpkeep;
	}

	public void setGasUpkeep(int value)
	{
		gasUpkeep = value;
	}

	public int getNumMoves()
	{
		return moves;
	}



	public virtual Weapon getWeapon(bool isPrimary)
	{
		if (isPrimary)
		{
			return primary;
		}
		return secondary;
	}

	public virtual bool canRetaliate(Unit atker)
	{

		if (atker == null || atker.getTeam() == this.getTeam())
		{
			return false;
		}
		// primary;
		int distance = Position.getDistance(getPosition(), atker.getPosition());
		if (primary.canRetal() && primary.ammo > 0 && primary.maxRange >= distance && primary.minRange <= distance && primary.radius == 0)
		{
			return true;
		}
		else if (secondary.canRetal() && secondary.ammo > 0 && secondary.maxRange >= distance && secondary.minRange <= distance && secondary.radius == 0)
		{
			return true;
		}

		return false;
	}

	public Position getPosition()
	{
		return new Position(this.row, this.col);
	}

	public void setCanCapture(bool cap)
	{
		cCapture = cap;
	}

	public virtual bool hasCaptureAbility()
	{
		return canCapture();
	}

	public virtual bool canCapture()
	{
		return cCapture && !hAttacked && (!hMoved || cMoveAndAttack);
	}

	public virtual void renew()
	{
		hMoved = false;
		hAttacked = false;
	}

	public bool getFirstWeaponChoice()
	{
		if (primary.hasAmmo())
		{
			return true;
		}
		return false;	
	}

	public virtual bool canDamage(Tile tile, bool isPrimary)
	{

		if (tile.getUnit() == null)
		{
			return false;
		}

		if (isPrimary && primary.ammo > 0)
		{
			return true;
		}
		else if (!isPrimary && secondary.maxAmmo > 0)
		{
			return true;
		}
		return false;
	}

	public virtual double getCapSpeed()
	{
		return capSpeed;
	}

	public void setCapSpeed(double c)
	{
		capSpeed = c;
	}


	public virtual void upkeepGas()
	{
		if (maxGas != 0)
		{
			gas -= gasUpkeep;
		}
	}

	public virtual void payGas(int distance)
	{
		if (maxGas != 0)
		{
			gas -= distance;
		}
	}


	public int getMaxGas()
	{
		return maxGas;
	}

	public void setMaxGas(int value)
	{
		maxGas = value;
		gas = maxGas;
	}

	public void setDieOnNoGas(bool value)
	{
		dieOnGasless = value;
	}


	public int getGas()
	{
		return gas;
	}

	public double getPercentHealth()
	{
		return (double)health/maxHealth;
	}
	public override string ToString()
	{
		string s = this.getName() + " ";
		if (this.getPosition() != null)
		{
			s += this.getPosition().ToString();
		}
		return s;
	}

	public void setArmor(DamageType value)
	{
		armor = value;
	}

	public DamageType getArmor()
	{
		return armor;
	}

}