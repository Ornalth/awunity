using System.Collections.Generic;

public interface Observer
{
	void update(object obj, string reason);
	void update(List<Position> positions);
	void update(Position position);
	void update(Player player);
	
	void gameFinished(int winner);
	void newTurn();
}
