﻿using UnityEngine;
using System.Collections;

public class DamageHintView : MonoBehaviour {

	SpriteRenderer backgroundRenderer;
	TypogenicText damageText;
	
	void Awake () {
		TypogenicText[] texts = GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText text in texts)
		{
			if (text.name.Equals("DamageText"))
			{
				damageText = text;
			}
		}
	}
	
	public void setDamage(int dmg)
	{
		damageText.Text = "" + dmg;
	}
}
