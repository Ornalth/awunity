using System;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using System.Text;
using System.IO;  
using System.Linq;

public class Board
{

	public const string UPDATE_REASON_END_TURN = "TURN END";
	public const string UPDATE_REASON_MOVE = "MOVE";
	public const string UPDATE_REASON_REPAIR = "REPAIR";
	public const string UPDATE_REASON_KILL = "KILL";
	private static Board INSTANCE;
	private static Tile[,] board;
	internal int turn = Constants.TEAM_BLUE;
	internal int day = 1;
	internal List<List<Building>> allBuildings = new List<List<Building>>();
	internal Player[] players = new Player[2];
	internal List<List<Unit>> allUnits = new List<List<Unit>>();
	internal List<Observer> observers = new List<Observer>();
	private static Building[] hqs = new Building[2];
	private bool gameOver = true;

	private bool abilActivated = false;
	private List<CombatModifierEffectAbility> turnEffects;

	//Undo package
	Tile oldTile = null;
	Tile newTile = null;
	bool canUndo = false;


	// ArrayList<Building> buildings = new ArrayList<Building>();
	private Board()
	{
		turnEffects = new List<CombatModifierEffectAbility>();
	}

	public static Board getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new Board();
		}

		return INSTANCE;
	}

	public virtual Tile[,] getBoard()
	{
		return board;
	}

	public virtual void newGame()
	{
		gameOver = false;
		players[0] = new Player(Constants.TEAM_BLUE, new GroundCO());//new InfluenceComputerV2(Constants.TEAM_BLUE);
		players[1] = new Player(Constants.TEAM_RED, new GroundCO());
		// players[0] = new InfluenceComputer(Constants.TEAM_BLUE);
		// players[1] = new InfluenceComputerV2(Constants.TEAM_RED);
		turn = 0;
		allBuildings.Add(new List<Building>());
		allBuildings.Add(new List<Building>());
		allBuildings.Add(new List<Building>());
		allUnits.Add(new List<Unit>());
		allUnits.Add(new List<Unit>());
		allUnits.Add(new List<Unit>());

		initBoardFromFile();
		beginGame();
	}

	public void addCombatModifierEffect(CombatModifierEffectAbility ability)
	{
		turnEffects.Add(ability);
	}

	public List<ICombatModifierEffect> getTurnEffectsForPlayer(Player player)
	{
		int target = player.getTeam();
		List<ICombatModifierEffect> effects = new List<ICombatModifierEffect>();
		foreach (CombatModifierEffectAbility abil in turnEffects)
		{
			if (abil.affectsTargetTeam(target))
			{
				MyLogger.log("FOUND EFFECT");
				effects.Add(abil.getEffect());
			}
		}
		return effects;
	}

	public virtual void beginGame()
	{
		if (players[0] is Computer)
		{
			Thread thread = new Thread(() =>
			{
				((Computer) players[0]).makeMove();

			});
			thread.Start();

		}
	}

	public bool isHumanPlayerTurn()
	{
		return !(players[turn] is Computer);
	}

	private void initBoardFromFile()
	{
		board = new Tile[Constants.width, Constants.height];
		
		StreamReader reader = null;
		try
		{
			reader = new StreamReader(Constants.boardFile, Encoding.Default);
			int row = 0;

			// Tile 1 = type
			// 2 = team if building
			// 3 = unit
			// 4 = unit team
			
			while (true)
			{
				string boardRow = reader.ReadLine();
				if (boardRow == null)
				{
					break;
				}
				string[] tileStrs = boardRow.Split(null);
				int column = 0;
				foreach (string tileStr in tileStrs)
				{
					int currentChar = 0;
					Tile tile = TileFactory.tileFromChar(tileStr[currentChar], row, column);
					currentChar++;
					if (tile.isBuilding())
					{
						Building building = (Building) tile;
						building.setTeam(tileStr[currentChar] - '0');
						currentChar++;
						board[row,column] = building;
						allBuildings[building.getTeam()].Add(building);
						if (building is Headquarters)
						{
							hqs[building.getTeam()] = building;
						}
					}
					else
					{
						board[row,column] = tile;
					}

					// TODO init with untis on board.
					// Has unit
					/*
					if (tileStr.length() > currentChar + 1)
					{
						Unit unit = UnitFactory.createUnit(
								tileStr.charAt(currentChar), row, column,
								tileStr.charAt(currentChar + 1) - '0');

						currentChar += 2;

					}
					 */

					column++;
				}
				row++;
			}
			reader.Close();
		}
		// catch (FileNotFoundException e)
		// {
		// e.printStackTrace();
		// }
		catch (Exception e)
		{
			MyLogger.log("ERROR " + e.ToString());
			if (reader != null)
			{
				reader.Close();
			}
		}
	}

	public virtual void printBoard()
	{
		for (int row = 0; row < Constants.width; row++)
		{
			for (int col = 0; col < Constants.height; col++)
			{
				//MyLogger.log(board[row,col].ToString() + " ");
			}
			//MyLogger.log();
		}
	}

	// --------------------------- GAME RELATED------------------------------

	public virtual void endTurn()
	{
		if (gameOver)
		{
			return;
		}
		turn ^= 1;
		if (turn == Constants.TEAM_BLUE)
		{
			day++;
		}


		//Ability"RElated stuff
		List<CombatModifierEffectAbility> expiredAbils = new List<CombatModifierEffectAbility>();
		foreach (CombatModifierEffectAbility effect in turnEffects)
		{
			effect.decreaseDuration();
			if (effect.isFinished())
			{
				expiredAbils.Add(effect);
			}
		}
		turnEffects = turnEffects.Except(expiredAbils).ToList();
		abilActivated = false;


		List<Position> updates = restoreBuildings();
		generateGold();

		List<Unit> units = allUnits[turn];
		List<Unit> deadUnits = new List<Unit>();
		List<Unit> repairedUnits = new List<Unit>();
		foreach (Unit unit in units)
		{
			unit.renew();
			unit.checkPromotions();
			unit.upkeepGas();

			Tile tile = getTile(unit);
			if (tile.isBuilding() && unit != null && unit.getTeam() == turn && unit.getTeam() == ((Building) tile).getTeam())
			{
				if (unit.getHealth() < unit.getMaxHealth())
				{

					int cost = UnitFactory.getRepairCost(unit);
					if (players[turn].getGold() > cost)
					{
						players[turn].pay(cost);
						unit.repair();
						repairedUnits.Add(unit);
					}
				}
				unit.resupply();
			}

			if (unit.isDead())
			{
				tile.setUnit(null);
				deadUnits.Add(unit);
			}
			updates.Add(unit.getPosition());
		}

		units = units.Except(deadUnits).ToList();

		HashSet<Position> sett = new HashSet<Position>(updates);
		List<Position> list = new List<Position>(sett);



		notifyObservers(list);
		notifyObservers(getCurrentPlayer());


		notifyObservers(getCurrentPlayer(), Board.UPDATE_REASON_END_TURN);
		notifyObservers(repairedUnits, Board.UPDATE_REASON_REPAIR);
		notifyNewTurn();
		if (getCurrentPlayer() is Computer)
		{
			Thread thread = new Thread(() =>
			{
				((Computer) getCurrentPlayer()).makeMove();

			});
			thread.Start();
		}
	}

	public virtual List<Position> getPossibleMoves(Tile t)
	{
		List<Position> possibleMoves = new List<Position>();
		if (t.hasUnit())
		{
			getPossibleMoves(t.getUnit(), t.getUnit().getNumMoves(), t.getPosition(), possibleMoves);
			possibleMoves.Remove(t.getPosition());
			return possibleMoves;
		}
		else
		{
			return possibleMoves;
		}
	}

	public virtual List<Position> getTargets(Tile t, bool isPrimary)
	{
		Unit unit = t.getUnit();
		if (unit == null)
		{
			return new List<Position>();
		}

		Weapon weapon;

		if (isPrimary)
		{
			weapon = unit.getPrimaryWeapon();
		}
		else
		{
			weapon = unit.getSecondaryWeapon();
		}

		if (weapon == null || !weapon.hasAmmo())
		{
			return new List<Position>();
		}

		int maxRange = weapon.maxRange;
		int minRange = weapon.minRange;
		Position thisPos = new Position(t.row, t.col, maxRange);
		List<Position> targets = getTargets(unit, thisPos, minRange, maxRange, weapon.radius);
		// getTargets(unit, maxRange, thisPos, positionHolder, targets,
		// isPrimary);
		filterMinRange(thisPos, targets, minRange);
		return targets;
	}

	public virtual List<Position> getSimulatedTargets(Unit unit, Tile origin, bool isPrimary)
	{
		if (unit == null)
		{
			return new List<Position>();
		}

		Weapon weapon;

		if (isPrimary)
		{
			weapon = unit.getPrimaryWeapon();
		}
		else
		{
			weapon = unit.getSecondaryWeapon();
		}

		if (weapon == null || !weapon.hasAmmo())
		{
			return new List<Position>();
		}

		int maxRange = weapon.maxRange;
		int minRange = weapon.minRange;

		Position thisPos = new Position(origin.row, origin.col, maxRange);
		List<Position> targets = getTargets(unit, thisPos, minRange, maxRange, weapon.radius);
		// getTargets(unit, maxRange, thisPos, positionHolder, targets,
		// isPrimary);
		filterMinRange(thisPos, targets, minRange);
		return targets;
	}

	private void filterMinRange(Position unitPos, List<Position> targets, int minRange)
	{

		List<Position> outOfRange = new List<Position>();
		foreach (Position targetPos in targets)
		{
			if (minRange > Position.getDistance(unitPos, targetPos))
			{
				outOfRange.Add(targetPos);
			}
		}

		targets = targets.Except(outOfRange).ToList();
	}

	public virtual void attack(Tile atker, Tile defender, bool isPrimary)
	{
		List<Position> changedPos = new List<Position>();
		changedPos.Add(atker.getPosition());
		AttackObject atkObj = atker.attack(defender, isPrimary);
		Unit atkingUnit = atker.getUnit();
		List<Unit> deadUnits = new List<Unit>();

		if (atkObj.error == null)
		{


			// Splash
			for (int row = defender.row - atkObj.radius; row <= defender.row + atkObj.radius; row++)
			{
				for (int col = defender.col - atkObj.radius; col <= defender.col + atkObj.radius; col++)
				{

					// Off board
					if (row < 0 || row >= Constants.height || col < 0 || col >= Constants.width)
					{
						continue;
					}
					else
					{
						Tile tile = board[row,col];
						Position thisPos = new Position(row, col);
						if (Position.getDistance(defender.getPosition(), thisPos) <= atkObj.radius)
						{

							// Splash actually hit a unit!
							if (tile.hasUnit())
							{
								Unit unit = tile.getUnit();
								dealCombatDamage(atkingUnit, unit, atkObj);

								if (unit.isDead())
								{
									deadUnits.Add(unit);

									atkingUnit.addKill(unit);
									tile.setUnit(null);
								}
								else
								{
									if (unit.canRetaliate(atkingUnit))
									{
										AttackObject counterAtkObj = unit.retaliate(atker);
										dealCombatDamage(unit, atkingUnit, counterAtkObj);
								

										if (atkingUnit.isDead())
										{
											deadUnits.Add(atkingUnit);
											unit.addKill(atkingUnit);
											atker.setUnit(null);
										}
									}
								}

								changedPos.Add(tile.getPosition());
							}
						}
					}
				}
			}

			foreach (Unit unit in deadUnits)
			{
				List<Unit> teamUnitList = allUnits[unit.getTeam()];
				teamUnitList.Remove(unit);
			}
		}
		else
		{
			MyLogger.log(atkObj.error);
		}

		canUndo = false;
		notifyObservers(deadUnits, UPDATE_REASON_KILL);
		notifyObservers(changedPos);
		// Do splash.
	}

	private void dealCombatDamage(Unit attacker, Unit defender, AttackObject atkObj)
	{
		int baseDmg = atkObj.damage;
		atkObj.damage = baseDmg + MiscUtils.randInt(Constants.LUCK_MIN, Constants.LUCK_MAX);
		
		double defenderOriginalHealth = defender.getPercentHealth();
		defender.takeDamage(atkObj, attacker);	
		double defenderPostHealth = defender.getPercentHealth();

		atkObj.damage = baseDmg;

		Player attackingPlayer = getPlayer(attacker.getTeam());
		Player defendingPlayer = getPlayer(defender.getTeam());
		

		double attackerAP = 
		Math.Round(UnitFactory.getCost(defender) * 
			(defenderOriginalHealth - defenderPostHealth) / 0.5, 1);
		
		MyLogger.log("Added " + attackerAP + " points for player " + attackingPlayer.getTeam());
		attackingPlayer.addAbilityPoints(attackerAP);
		double defenderAP = Math.Round(0.25 * attackerAP, 1);
		MyLogger.log("Added " + defenderAP + " points for player " + defendingPlayer.getTeam());
		defendingPlayer.addAbilityPoints(defenderAP);

	}

	// TODO ERROR CHECK THIS, NAH.
	public void move(Tile src, Tile des)
	{
		if (src.Equals(des))
		{
			return;
		}


		List<Position> positions = new List<Position>(); // Animation
		positions.Add(src.getPosition());
		positions.Add(des.getPosition());
		List<Position> animatePositions = findPath(src, des);

		Unit unit = src.getUnit();

		//Failed move?...
		if (!unit.move())
		{
			return;
		}

		Position pos = des.getPosition();
		unit.col = pos.col;
		unit.row = pos.row;
		unit.payGas(Position.getDistance(src.getPosition(), des.getPosition()));
		des.setUnit(unit);
		src.setUnit(null);


		oldTile = src;
		newTile = des;
		canUndo = true;


		notifyObservers(animatePositions, UPDATE_REASON_MOVE);
		notifyObservers(positions);

		//notifyObservers(unit);
		MyLogger.log("Unit Moved");
	}


	public void undoMove()
	{
		if (!canUndo)
		{
			MyLogger.log("CANT UNDO");
			return;
		}
		List<Position> positions = new List<Position>(); 
		positions.Add(newTile.getPosition());
		positions.Add(oldTile.getPosition());

		Unit unit = newTile.getUnit();
		newTile.setUnit(null);
		oldTile.setUnit(unit);
		unit.undoMove();
		Position pos = oldTile.getPosition();
		unit.col = pos.col;
		unit.row = pos.row;


		canUndo = false;

		notifyObservers(positions);
	}

	private List<Position> findPath(Tile src, Tile des)
	{
		// ArrayList<Position> positions = new ArrayList<Position>();
		// positions.add(src.getPosition());

		Unit unit = src.getUnit();
		//PriorityQueue<Position> frontier = new PriorityQueue<Position>(10, new ComparatorAnonymousInnerClassHelper(this));
		SortedList frontier = new SortedList(new ComparatorAnonymousInnerClassHelper(this), 10);

		Dictionary<Position, int?> costMap = new Dictionary<Position, int?>();

		// ArrayList<Position> closed = new ArrayList<Position>();
		Position start = src.getPosition();
		start.parent = null;
		frontier.Add(start,start);
		costMap[start] = 0;
		start.movesLeft = 0;
		Position goal = des.getPosition();
		Position current = null;
		while (frontier.Count > 0)
		{
			current = (Position)frontier.GetKey(0);
			frontier.Remove(current);


			if (current.Equals(goal))
			{
				break;
			}
			foreach (Position next in current.Neighbours)
			{
				int cost = 0;
				if (getTile(next).getUnit() != null)
				{
					if (!isFriendly(unit, getTile(next).getUnit()))
					{
						cost = 999;
					}
					else
					{
						cost = getTile(next).getMovementCost(unit);
					}
				}
				else
				{
					cost = getTile(next).getMovementCost(unit);
				}
				int? csharpplz = costMap[current];
				int new_cost = cost + (csharpplz ?? default(int));
				
				//prevCost = costMap[next]
				if (!costMap.ContainsKey(next) || new_cost < costMap[next])
				{
					costMap[next] = new_cost;
					next.movesLeft = new_cost + heuristic(next, goal);
					if (!frontier.Contains(next))
					{
						frontier.Add(next, next);
					}
					next.parent = current;
				}
			}
			// closed.add(current);
		}
		List<Position> path = new List<Position>();
		path.Add(current);
		while (!current.Equals(start))
		{
			current = current.parent;
			path.Add(current);
		}
		path.Reverse();
		return path;
	}

	private class ComparatorAnonymousInnerClassHelper : IComparer
	{
		private readonly Board outerInstance;

		public ComparatorAnonymousInnerClassHelper(Board outerInstance)
		{
			this.outerInstance = outerInstance;
		}


		public virtual int Compare(object arg0, object arg1)
		{
			Position pos0 = (Position)arg0;
			Position pos1 = (Position)arg1; 
			int m1 = pos0.movesLeft;
			int m2 = pos1.movesLeft;
			if (m1 > m2)
			{
				return 1;
			}
			else if (m1 < m2)
			{
				return -1;
			}
			return 0;
		}
	}

	private bool isFriendly(Unit unit, Unit unit2)
	{
		if (unit.getTeam() == unit2.getTeam())
		{
			return true;
		}
		return false;
	}

	private int heuristic(Position src, Position des)
	{
		return Math.Abs(src.row - des.row) + Math.Abs(src.col - des.col);
	}

	/// <summary>
	/// Returns the changed positions to update
	/// </summary>
	private List<Position> restoreBuildings()
	{
		List<Position> updates = new List<Position>();
		foreach (List<Building> teamsBuildings in allBuildings)
		{
			foreach (Building building in teamsBuildings)
			{
				// TODO maybe change later, only restore health if no unit on
				// top.
				if (!building.hasUnit() && building.getHealth() != Constants.BUILDING_MAX_HEALTH)
				{
					updates.Add(building.getPosition());
					building.restoreHealth();
				}
			}
		}
		return updates;

	}

	private void generateGold()
	{
		List<Building> teamsBuildings = allBuildings[turn];
		int amount = teamsBuildings.Count * Constants.GOLD_PER_BUILDING;
		players[turn].addGenerationGold(amount);
	}

	private List<Position> getTargets(Unit unit, Position origin, int minRange, int maxRange, int radius)
	{
		List<Position> positions = Position.getAllPositionsUpTo(origin, minRange, maxRange);
		if (radius == 0)
		{
			for (int i = positions.Count - 1; i >= 0; i--)
			{
				Unit target = getTile(positions[i]).getUnit();
				if (target == null || unit.getTeam() == target.getTeam())
				{
					positions.RemoveAt(i);
				}
			}

		}
		return positions;
	}
	
	private void getPossibleMoves(Unit unit, int movesLeft, Position current, List<Position> prevMoves)
	{
		int col = current.col;
		int row = current.row;

		// RIGHT
		if (col + 1 < Constants.width)
		{
			Tile tile = board[row,col + 1];

			int cost = tile.getMovementCost(unit);
			if (movesLeft - cost >= 0)
			{
				Position pos = new Position();
				pos.col = col + 1;
				pos.row = row;
				int newMovesLeft = movesLeft - cost;
				pos.movesLeft = newMovesLeft;
				int index = prevMoves.IndexOf(pos);
				if (index != -1)
				{
					Position oldPos = prevMoves[index];
					if (pos.movesLeft > oldPos.movesLeft)
					{
						prevMoves.Remove(oldPos);
						prevMoves.Add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
					}
				}
				else
				{
					if (tile.getUnit() == null)
					{
						prevMoves.Add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);

					}
					else
					{
						if (tile.getUnit().getTeam() == unit.getTeam())
						{
							getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
						}
					}
				}
			}
		}

		// LEFT
		if (col - 1 >= 0)
		{
			Tile tile = board[row,col - 1];

			int cost = tile.getMovementCost(unit);
			if (movesLeft - cost >= 0)
			{
				Position pos = new Position();
				pos.col = col - 1;
				pos.row = row;
				int newMovesLeft = movesLeft - cost;
				pos.movesLeft = newMovesLeft;
				int index = prevMoves.IndexOf(pos);
				if (index != -1)
				{
					Position oldPos = prevMoves[index];
					if (pos.movesLeft > oldPos.movesLeft)
					{
						prevMoves.Remove(oldPos);
						prevMoves.Add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
					}
				}
				else
				{
					if (tile.getUnit() == null)
					{
						prevMoves.Add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);

					}
					else
					{
						if (tile.getUnit().getTeam() == unit.getTeam())
						{
							getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
						}
					}
				}
			}
		}

		// UP

		if (row + 1 < Constants.height)
		{
			Tile tile = board[row + 1,col];

			int cost = tile.getMovementCost(unit);
			if (movesLeft - cost >= 0)
			{
				Position pos = new Position();
				pos.col = col;
				pos.row = row + 1;
				int newMovesLeft = movesLeft - cost;
				pos.movesLeft = newMovesLeft;
				int index = prevMoves.IndexOf(pos);
				if (index != -1)
				{
					Position oldPos = prevMoves[index];
					if (pos.movesLeft > oldPos.movesLeft)
					{
						prevMoves.Remove(oldPos);
						prevMoves.Add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
					}
				}
				else
				{
					if (tile.getUnit() == null)
					{
						prevMoves.Add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);

					}
					else
					{
						if (tile.getUnit().getTeam() == unit.getTeam())
						{
							getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
						}
					}
				}
			}
		}

		// DOWN
		if (row - 1 >= 0)
		{
			Tile tile = board[row - 1,col];

			int cost = tile.getMovementCost(unit);
			if (movesLeft - cost >= 0)
			{
				Position pos = new Position();
				pos.col = col;
				pos.row = row - 1;
				int newMovesLeft = movesLeft - cost;
				pos.movesLeft = newMovesLeft;
				int index = prevMoves.IndexOf(pos);
				if (index != -1)
				{
					Position oldPos = prevMoves[index];
					if (pos.movesLeft > oldPos.movesLeft)
					{
						prevMoves.Remove(oldPos);
						prevMoves.Add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
					}
				}
				else
				{
					if (tile.getUnit() == null)
					{
						prevMoves.Add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);

					}
					else
					{
						if (tile.getUnit().getTeam() == unit.getTeam())
						{
							getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
						}
					}
				}
			}
		}
	}

	public virtual Tile getTile(Position pos)
	{
		return board[pos.row,pos.col];
	}

	public static int getDefense(Unit u)
	{

		return board[u.row,u.col].getDefense();
	}

	// public getters

	public int getCurrentTeam()
	{
		return turn;
	}

	public Player getPlayer(int team)
	{
		return players[team];
	}

	public Player getCurrentPlayer()
	{
		return players[turn];
	}

	public int getGold(int player)
	{
		return players[player].getGold();
	}

	public int getCurrentPlayerGold()
	{
		return getCurrentPlayer().getGold();
	}

	public void purchase(Unit u, Tile t)
	{
		Player player = getCurrentPlayer();
		if (player.pay(UnitFactory.getCost(u)))
		{
			Unit unit = UnitFactory.createUnit(u, t.row, t.col, player.getTeam());
			t.setUnit(unit);

			// Update all unit state.
			List<Unit> units = allUnits[turn];
			units.Add(unit);
			MyLogger.log("????");
			notifyObservers(t.getPosition());
			notifyObservers(player);
		}
	}

	// ---- OBSERVER STUFF
	public void notifyNewTurn()
	{
		foreach (Observer obs in observers)
		{
			obs.newTurn();
		}
	}

	public void notifyObservers(List<Position> updates)
	{
		foreach (Observer obs in observers)
		{
			obs.update(updates);
		}
	}

	public void notifyObservers(Position updates)
	{
		notifyObservers(new List<Position>(){updates});
	}

	public void notifyGameFinished(int winner)
	{
		foreach (Observer obs in observers)
		{
			obs.gameFinished(winner);
		}
	}

	public void notifyObservers(Player player)
	{
		foreach (Observer obs in observers)
		{
			obs.update(player);
		}
	}


	public virtual void subscribe(Observer obs)
	{
		observers.Add(obs);
	}

	public virtual void finishedTile(Tile tile)
	{
		Unit unit = tile.getUnit();
		if (unit.hasMoved())
		{
			unit.setHasAttacked(true);
			canUndo = false;
			notifyObservers(tile.getPosition());
		}
	}


	private void notifyObservers(object objs, string reason)
	{
		foreach (Observer obs in observers)
		{
			obs.update(objs, reason);
		}
	}

	public bool canCapture(Tile origin)
	{
		return origin != null && 
		origin.getUnit() != null && 
		origin.getUnit().canCapture() && 
		origin.isBuilding() &&
		((Building)origin).getTeam() != origin.getUnit().getTeam();
	}

	/// <summary>
	/// returns if valid call
	/// </summary>
	public virtual bool cap(Tile origin)
	{

		if (origin != null && origin.getUnit() != null && origin.getUnit().canCapture() && origin.isBuilding())
		{
			// TODO real cap logic.
			Building build = (Building)(origin);
			Unit unit = origin.getUnit();
			if (build.getTeam() != unit.getTeam())
			{
				if (build.cap())
				{
					List<Building> removeList = allBuildings[build.getTeam()];
					removeList.Remove(build);
					build.setTeam(unit.getTeam());

					List<Building> buildList = allBuildings[build.getTeam()];
					buildList.Add(build);

					if (build is Headquarters)
					{
						setGameOver(build.getTeam());
						return false;
					}
				}
				unit.setHasAttacked(true);
				unit.setHasMoved(true);
				notifyObservers(origin.getPosition());
				return true;
			}

		}
		MyLogger.log("BAD CAP CALL.");
		return false;
		// otherwise. bad call
	}

	public bool canActivateRegularAbility()
	{
		Player player = getCurrentPlayer();
		return !abilActivated && player.canPayAbility();
	}

	public bool canActivateSuperAbility()
	{
		Player player = getCurrentPlayer();
		return 	!abilActivated && player.canPaySuperAbility();
	}

	public void activateRegularAbility()
	{
		Player player = getCurrentPlayer();
		if (canActivateRegularAbility())
		{
			player.activateCOAbility();
			abilActivated = true;
			MyLogger.log("Activated co. abil");
			List<Unit> teamUnitList = allUnits[player.getTeam()];
			List<Position> updatedUnits = getPositionsForUnits(teamUnitList);
			notifyObservers(updatedUnits);
		}


	}

	public void activateSuperAbility()
	{
		Player player = getCurrentPlayer();
		if (canActivateSuperAbility())
		{
			player.activateSuperCOAbility();
			abilActivated = true;
			MyLogger.log("Activated super co. abil");
			List<Unit> teamUnitList = allUnits[player.getTeam()];
			List<Position> updatedUnits = getPositionsForUnits(teamUnitList);
			notifyObservers(updatedUnits);
		}


	}
	private List<Position> getPositionsForUnits(List<Unit> units)
	{
		List<Position> positions = new List<Position>();
		foreach (Unit u in units)
		{
			positions.Add(u.getPosition());
		}
		return positions;
	}
	private void setGameOver(int i)
	{
		gameOver = true;
		notifyGameFinished(i);
		MyLogger.log("Game has ended. Player " + i + " has won.");
		// Player i wins.
		// notifyObservers();
	}

	public virtual List<Unit> getUnitsForTeam(int team)
	{
		return allUnits[team];
	}

	public virtual List<Building> getBuildingsForTeam(int team)
	{
		return allBuildings[team];
	}

	public virtual Tile getTile(Unit unit)
	{
		Position pos = unit.getPosition();
		return board[pos.row,pos.col];
	}

	public virtual Unit getNextActiveUnit(int team)
	{
		List<Unit> units = allUnits[team];
		foreach (Unit unit in units)
		{
			if (unit.canMove())
			{
				return unit;
			}
		}
		return null;
	}

	public virtual void sortUnitsByDistanceFromHq(int team)
	{
		List<Unit> units = allUnits[team];

		List<Building> build = allBuildings[team];
		units.Sort(UNIT_DISTANCE_FROM_HQ_SORT);
	}

	public virtual void sortUnitsByBeginAbleToCap(int team)
	{
		List<Unit> units = allUnits[team];

		List<Building> build = allBuildings[team];
		units.Sort(UNIT_CAP_ABILITY_SORT);
	}

	public virtual void stopUnit(Unit unit)
	{

		unit.move();
	}

	private static readonly IComparer<Unit> UNIT_DISTANCE_FROM_HQ_SORT = new ComparatorAnonymousInnerClassHelper2();

	private class ComparatorAnonymousInnerClassHelper2 : IComparer<Unit>
	{
		public ComparatorAnonymousInnerClassHelper2()
		{
		}


		public virtual int Compare(Unit u1, Unit u2)
		{
			int distance1 = Position.getDistance(u1.getPosition(), hqs[u1.getTeam()].getPosition());
			int distance2 = Position.getDistance(u2.getPosition(), hqs[u2.getTeam()].getPosition());
			if (distance1 > distance2)
			{
				return 1;
			}
			else if (distance1 == distance2)
			{
				return 0;
			}
			else
			{
				return -1;
			}
		}

	}

	// Cappers in front.
	private static readonly IComparer<Unit> UNIT_CAP_ABILITY_SORT = new ComparatorAnonymousInnerClassHelper3();

	private class ComparatorAnonymousInnerClassHelper3 : IComparer<Unit>
	{
		public ComparatorAnonymousInnerClassHelper3()
		{
		}


		public virtual int Compare(Unit u1, Unit u2)
		{
			if (u1.canCapture() && !u2.canCapture())
			{
				return -1;
			}
			else if (u1.canCapture() == u2.canCapture())
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}

	}

	public virtual Position getOtherTeamHQPosition(int team)
	{
		Building build = hqs[team ^ 1];
		return build.getPosition();
	}

	public int getDay()
	{
		return day;
	}
}