﻿using System.Collections.Generic;

public class Move
{
	internal Position attack = null;
	internal Position move = null;

	internal int scoreMove = -99999999;
	internal int scoreAction = -4234934;

	internal bool isPrimary = false;

	internal bool isCap = false;
	//Collections.sort(paidBills, Bill.SORTER);
	public static IComparer<Move> SORT_BY_MOVE_SCORE_DESC = new ComparatorAnonymousInnerClassHelper();

	private class ComparatorAnonymousInnerClassHelper : IComparer<Move>
	{
		public ComparatorAnonymousInnerClassHelper()
		{
		}

		public virtual int Compare(Move m1, Move m2)
		{
			if (m1.scoreMove > m2.scoreMove)
			{
				return -1;
			}
			else if (m1.scoreMove == m2.scoreMove)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}

}