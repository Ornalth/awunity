﻿


public class GroundCO : GenericCO
{


	public override double getGroundWeaponDamageModifier()
	{
		return 1.1;
	} 

	public override double getGroundWeaponDefenseModifier()
	{
		return 1.1;
	} 


	public override double getAbilityCost()
	{
		return 30.0;
	}

	public override double getSuperAbilityCost()
	{
		return 60.0;
	}

	public override void doAbility(Player owner)
	{
		Board board = Board.getInstance();
		currentAP -= getAbilityCost();
		CombatModifierEffectAbility abil = new CombatModifierEffectAbility(owner.getTeam(), 2, true, false, new GroundCOBasicAbility());
		board.addCombatModifierEffect(abil);
	}

	public override void doSuperAbility(Player owner)
	{
		Board board = Board.getInstance();
		currentAP -= getSuperAbilityCost();
		CombatModifierEffectAbility abil = new CombatModifierEffectAbility(owner.getTeam(), 2, true, false, new GroundCOSuperAbility());
		board.addCombatModifierEffect(abil);
	}
}


public class GroundCOBasicAbility :  CombatModifierEffectAdapter
{
	public override double getGroundWeaponDamageModifier()
	{
		return 1.15;
	} 
	
	public override double getGroundWeaponDefenseModifier()
	{
		return 1.15;
	} 
}


public class GroundCOSuperAbility :  CombatModifierEffectAdapter
{
	public override double getGroundWeaponDamageModifier()
	{
		return 1.5;
	} 
	
	public override double getGroundWeaponDefenseModifier()
	{
		return 1.5;
	} 
}
