﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class DetailsViewScript : MonoBehaviour {

	public interface DetailsViewScriptListener
	{
		void captureBuildingRequest(DetailsViewScript scr);
		void weaponSelected(bool isPrimary);
		void attackRequest(DetailsViewScript scr);
	}

	Text tileNameText;
	Text tileDataText;
	Image tileImage;

	Text unitNameText;
	Text unitHealthText;
	Text unitGasText;
	Image unitImage;

	Text weaponName1Text;
	Text weaponAmmo1Text;
	Text weaponDetails1Text;

	Text weaponName2Text;
	Text weaponAmmo2Text;
	Text weaponDetails2Text;

	Image primaryWeaponBg;
	Image secondaryWeaponBg;

	Button primaryWeaponButton;
	Button secondaryWeaponButton;

	GameObject primaryWeaponPanel;
	GameObject secondaryWeaponPanel;

	Tile tile;

	Button capButton;
	Button attackButton;
	List<DetailsViewScriptListener> listeners = new List<DetailsViewScriptListener>();
	bool selectedWeaponIsPrimary = true;

	void Awake()
	{
		Text[] texts = GetComponentsInChildren<Text>();
		foreach (Text text in texts)
		{
			if (text.name.Equals("TileName"))
			{
				tileNameText = text;
			}
			else if (text.name.Equals("TileData"))
			{
				tileDataText = text;
			}
			else if (text.name.Equals("UnitName"))
			{
				unitNameText = text;
			}
			else if (text.name.Equals("Health"))
			{
				unitHealthText = text;
			}
			else if (text.name.Equals("Gas"))
			{
				unitGasText = text;
			}


			else if (text.name.Equals("WeaponName1"))
			{
				weaponName1Text = text;
			}
			else if (text.name.Equals("Ammo1"))
			{
				weaponAmmo1Text = text;
			}
			else if (text.name.Equals("Details1"))
			{
				weaponDetails1Text = text;
			}

			else if (text.name.Equals("WeaponName2"))
			{
				weaponName2Text = text;
			}
			else if (text.name.Equals("Ammo2"))
			{
				weaponAmmo2Text = text;
			}
			else if (text.name.Equals("Details2"))
			{
				weaponDetails2Text = text;
			}
		}

		Button[] buttons = GetComponentsInChildren<Button>();
		foreach (Button button in buttons)
		{
			if (button.name.Equals("CapButton"))
			{
				capButton = button;
				capButton.onClick.AddListener(capButtonClicked);
			}
			else if (button.name.Equals("AttackButton"))
			{
				attackButton = button;
				attackButton.onClick.AddListener(attackButtonClicked);
			}
			else if (button.name.Equals("WeaponPanel1"))
			{
				primaryWeaponButton = button;
				primaryWeaponButton.onClick.AddListener(primaryWeaponButtonClicked);
			}
			else if (button.name.Equals("WeaponPanel2"))
			{
				secondaryWeaponButton = button;
				secondaryWeaponButton.onClick.AddListener(secondaryWeaponButtonClicked);
			}
		}

		Image[] images = GetComponentsInChildren<Image>();
		foreach (Image image in images)
		{
			if (image.name.Equals("TileImage"))
			{
				tileImage = image;
			}
			else if (image.name.Equals("UnitImage"))
			{
				unitImage = image;
			}
			else if (image.name.Equals("WeaponPanel1"))
			{
				primaryWeaponBg = image;
			}
			else if (image.name.Equals("WeaponPanel2"))
			{
				secondaryWeaponBg = image;
			}
		}
		
		primaryWeaponPanel = primaryWeaponBg.gameObject;
		secondaryWeaponPanel = secondaryWeaponBg.gameObject;
		//setWeaponSelection(true);
		clearTileDetails();
	}

	public void subscribe(DetailsViewScriptListener listener)
	{
		listeners.Add(listener);
	}

	public void capButtonClicked()
	{
		foreach (DetailsViewScriptListener listener in listeners)
		{
			listener.captureBuildingRequest(this);
		}
	}

	public void attackButtonClicked()
	{
		foreach (DetailsViewScriptListener listener in listeners)
		{
			listener.attackRequest(this);
		}
	}

	public void setWeaponSelection(bool isPrimary)
	{
		selectedWeaponIsPrimary = isPrimary;
		//primaryWeaponButton.Select();
		primaryWeaponBg.color = isPrimary ? Color.red : Color.white;
		secondaryWeaponBg.color = isPrimary ? Color.white : Color.red;
		foreach (DetailsViewScriptListener listener in listeners)
		{
			listener.weaponSelected(isPrimary);
		}
	}

	//Check in here because elsewhere calls setWeaponSelection.

	public void primaryWeaponButtonClicked()
	{

		if (selectedWeaponIsPrimary)
		{
			return;
		}
		setWeaponSelection(true);

	}

	public void secondaryWeaponButtonClicked()
	{
		if (!selectedWeaponIsPrimary)
		{
			return;
		}
		setWeaponSelection(false);
	}

	public bool getWeaponSelection()
	{
		return selectedWeaponIsPrimary;
	}

	public void showTileDetails(Tile t)
	{
		tile = t;

		tileNameText.text = t.getDrawableName();
		/*if (t.isBuilding())
		{
			healthText.setText("Health" + ((Building) t).getHealth());
		}*/
		tileDataText.text = "Def" + t.getDefense();
		tileImage.sprite = ImageLibrary.getImage(t.getDrawableName());

		Board b = Board.getInstance();
		capButton.interactable = b.canCapture(t);

		if (t.getUnit() != null)
		{
			
			Unit unit = t.getUnit();

			unitNameText.text = unit.getName();
			unitHealthText.text = "Health: " + unit.getHealth() + "/"
					+ unit.getMaxHealth();
			unitGasText.text = "Gas: " + unit.getGas() + "/" + unit.getMaxGas();

			unitImage.sprite = ImageLibrary.getImage(unit.getDrawableName());
			
			Weapon primary = unit.getPrimaryWeapon();
		
			if (!primary.isDefaultWeapon())
			{
				weaponName1Text.text = primary.name;
				weaponAmmo1Text.text = "Ammo: " + primary.ammo + "/" + primary.maxAmmo;
				weaponDetails1Text.text = "Damage: " + primary.baseDamage + "\n" + "Range: " + primary.minRange + "-" + primary.maxRange;

				setWeaponSelection(true);
				ViewHelper.show(primaryWeaponPanel);
			}

			Weapon secondary = unit.getSecondaryWeapon();
			if (!secondary.isDefaultWeapon())
			{
				weaponName2Text.text = secondary.name;
				weaponAmmo2Text.text = "Ammo: " + secondary.ammo + "/" + secondary.maxAmmo;
				weaponDetails2Text.text = "Damage: " + secondary.baseDamage + "\n" + "Range: " + secondary.minRange + "-" + secondary.maxRange;
				ViewHelper.show(secondaryWeaponPanel);
			}
			/*


			origin.ammo1.setText("Ammo: " + unit.getPrimaryWeapon().ammo + "/"
					+ unit.getPrimaryWeapon().maxAmmo);
			origin.ammo2.setText("Ammo: " + unit.getSecondaryWeapon().ammo
					+ "/" + unit.getSecondaryWeapon().maxAmmo);

			
			Weapon p = unit.getPrimaryWeapon();
			primary.ammo.setText("Ammo: " + p.ammo + "/" + p.maxAmmo);
			primary.damage.setText("Damage: " + p.baseDamage);
			primary.name.setText(p.name);
			primary.radius.setText("Radius: " + p.radius);
			primary.range.setText("Range: " + p.minRange + "-" + p.maxRange);
			primary.type.setText("Type :" + p.type.getName());
			primary.setVisible(true);
			Weapon s = unit.getSecondaryWeapon();
			secondary.ammo.setText("Ammo: " + s.ammo + "/" + s.maxAmmo);
			secondary.damage.setText("Damage: " + s.baseDamage);
			secondary.name.setText(s.name);
			secondary.radius.setText("Radius: " + s.radius);
			secondary.range.setText("Range: " + s.minRange + "-" + s.maxRange);
			secondary.type.setText("Type :" + s.type.getName());
			secondary.setVisible(true);
*/
		}
		else
		{
			clearTileDetails();
		}
	}

	public Tile getTile()
	{
		return tile;
	}

	public void clearTileDetails()
	{

		tile = null;
		unitNameText.text = "";
		unitHealthText.text = "";
		unitGasText.text = "";
		unitImage.sprite = null;
		primaryWeaponBg.color = Color.white;
		secondaryWeaponBg.color = Color.white;
		ViewHelper.hide(primaryWeaponPanel);
		ViewHelper.hide(secondaryWeaponPanel);
	}
}
