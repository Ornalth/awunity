﻿
public class Forest : Tile
{
	protected internal Forest(int row, int col) : base(row, col)
	{
	}

	public override string ToString()
	{
		return "FOR";
	}

	protected internal override int getFootUnitCost()
	{
		return 1;
	}

	protected internal override int getTireUnitCost()
	{
		return 3;
	}

	protected internal override int getTreadUnitCost()
	{
		return 2;
	}

	protected internal override int getSeaUnitCost()
	{
		return 99999;
	}

	protected internal override int getAirUnitCost()
	{
		return 1;
	}

	public override int getDefense()
	{
		return 2;
	}
}