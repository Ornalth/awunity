using System.Collections.Generic;

public class CombatCoordinator
{

	public static int getDamage(Unit attacker, bool isPrimary, Unit defender)
	{

		Weapon weapon = attacker.getWeapon(isPrimary);
		if (defender == null && weapon.radius == 0)
		{
			return 0;
		}

		int damage = 0;

/*
		double attackMultiplier = 1.0;
		
		attackMultiplier *= attacker.getPercentHealth();
		//Weapon advs?
		attackMultiplier *= getWeaponDamageTypeMultiplier(weapon, defender);
		//Attacker rank.
		attackMultiplier *= getDamageMultiplierForRank(attacker);

		attackMultiplier *= getCOAttackMultiplier(attacker);

		double defenseMultiplier = 1.0;
		defenseMultiplier *= getTileDefenseMultiplier(defender);
		//Rank bonus defense
		defenseMultiplier *= getDefenseMultiplierForRank(defender);
		defenseMultiplier *= getCODefenseMultiplier(defender);
*/

		double attackMultiplier = getAllAttackMultipliers(attacker, isPrimary, defender);
		double defenseMultiplier = getAllDefenseMultipliers(defender);
		MyLogger.log("ATK MULT " + attackMultiplier + " DEF MULT " + defenseMultiplier);
		damage = (int)( weapon.baseDamage * attackMultiplier * (1.0/defenseMultiplier));

		return damage;
	}


	//Summers
	private static double getAllAttackMultipliers(Unit attacker, bool isPrimary, Unit defender)
	{

		Weapon weapon = attacker.getWeapon(isPrimary);
		double attackMultiplier = 1.0;
		
		attackMultiplier *= attacker.getPercentHealth();
		//Weapon advs?
		attackMultiplier *= getWeaponDamageTypeMultiplier(weapon, defender);
		//Attacker rank.
		attackMultiplier *= getDamageMultiplierForRank(attacker);

		attackMultiplier *= getCOAttackMultiplier(attacker);
		attackMultiplier *= getTurnEffectsAttackMultiplier(attacker);
		return attackMultiplier;
	}

	private static double getAllDefenseMultipliers(Unit defender)
	{
		double defenseMultiplier = 1.0;
		defenseMultiplier *= getTileDefenseMultiplier(defender);
		//Rank bonus defense
		defenseMultiplier *= getDefenseMultiplierForRank(defender);
		defenseMultiplier *= getCODefenseMultiplier(defender);
		defenseMultiplier *= getTurnEffectsDefenseMultiplier(defender);
		return defenseMultiplier;
	}



	//Singles

	private static double getTurnEffectsAttackMultiplier(Unit attacker)
	{
		Board board = Board.getInstance();
		Player player = board.getPlayer(attacker.getTeam());
		double multiplier = 1.0;
		List<ICombatModifierEffect> effects = board.getTurnEffectsForPlayer(player);
		foreach (ICombatModifierEffect effect in effects)
		{
			multiplier *= getModifierForUnit(attacker, effect);
		}
		return multiplier;
	}

	private static double getTurnEffectsDefenseMultiplier(Unit defender)
	{
		Board board = Board.getInstance();
		Player player = board.getPlayer(defender.getTeam());
		double multiplier = 1.0;
		List<ICombatModifierEffect> effects = board.getTurnEffectsForPlayer(player);
		foreach (ICombatModifierEffect effect in effects)
		{
			multiplier *= getModifierForUnit(defender, effect);
		}
		return multiplier;
	}

	private static double getCOAttackMultiplier(Unit attacker)
	{
		Board board = Board.getInstance();
		Player player = board.getPlayer(attacker.getTeam());
		return getModifierForUnit(attacker, player);
	}

	private static double getCODefenseMultiplier(Unit defender)
	{
		Board board = Board.getInstance();
		Player player = board.getPlayer(defender.getTeam());
		return getModifierForUnit(defender, player);
	}

	private static double getTileDefenseMultiplier(Unit defender)
	{
		if (defender == null)
		{
			return 1.0;
		}
		return (10.0 + Board.getDefense(defender)) / 10.0;
	}

	private static double getWeaponDamageTypeMultiplier(Weapon weapon, Unit defender)
	{
		if (defender == null || defender.getMovementType() == MovementType.AIR)
		{
			return 1.0;
		}
		return DamageType.getDamageMultiplier(weapon.type, defender.getArmor());
	}

	private static double getDamageMultiplierForRank(Unit unit)
	{
		double multiplier = 1.0;
		if (unit == null)
		{
			return multiplier;
		}
		UnitRank rank = unit.getRank();
		if (rank == UnitRank.SERGEANT)
		{
			multiplier = 1.10;
		}
		else if (rank == UnitRank.LIEUTENANT)
		{
			multiplier = 1.20;
		}
		else
		{
			multiplier = 1.0;
		}
		return multiplier;
	}

	private static double getDefenseMultiplierForRank(Unit unit)
	{
		double multiplier = 1.0;
		if (unit == null)
		{
			return multiplier;
		}
		UnitRank defenderRank = unit.getRank();
		if (defenderRank == UnitRank.SERGEANT)
		{
			multiplier *= 1.1;
		}
		else if (defenderRank == UnitRank.LIEUTENANT)
		{
			multiplier *= 1.2;
		}
		else
		{
			multiplier *= 1.0;
		}
		return multiplier;
	}









	//Util

	private static double getModifierForUnit(Unit unit, ICombatModifierEffect effect)
	{
		if (unit.getMovementType() == MovementType.AIR)
		{
			return effect.getAirWeaponDamageModifier();
		}
		else if (unit.getMovementType() == MovementType.SEA)
		{
			return effect.getSeaWeaponDamageModifier();
		}
		else// if (attacker is GroundUnit)
		{
			MyLogger.log("EFF " + effect.getGroundWeaponDamageModifier());
			return effect.getGroundWeaponDamageModifier();
		}
		return 1.0;
	}

	
}