﻿using UnityEngine;
using System.Collections;

public class ScrollableAreaScript : MonoBehaviour {

	public interface ScrollableAreaListener
	{
		void scrolledBy(ScrollableAreaScript scr, float deltaX, float deltaY);
		void stoppedScroll(ScrollableAreaScript scr);
	}

	Vector2 initialPoint;
	private void calculateContentSize()
	{

	}

	private void setSize (float width, float height)
	{

	}

	private ScrollableAreaListener listener;
	private Color mouseOverColor = Color.blue;
	private Color originalColor = Color.yellow;
	private bool dragging = false;
	private float distance;
	
	void OnMouseEnter()
	{
		//renderer.material.color = mouseOverColor;
	}
	
	void OnMouseExit()
	{
		//renderer.material.color = originalColor;
	}
	
	void OnMouseDown()
	{
		//distance = Vector3.Distance(transform.position, Camera.main.transform.position);
		Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		initialPoint = mousePosition;
		dragging = true;
	}
	
	void OnMouseUp()
	{
		dragging = false;
		if (listener != null)
		{
			listener.stoppedScroll(this);
		}
	}
	
	void Update()
	{
		if (dragging)
		{
			Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			scrollBy(mousePosition.x - initialPoint.x, mousePosition.y - initialPoint.y);
			//Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			//Vector3 rayPoint = ray.GetPoint(distance);
			//rayPoint.z = transform.position.z;
			//transform.position = rayPoint;
		}
	}

	public void setListener( ScrollableAreaListener s)
	{
		listener = s;
	}

	private void scrollBy (float deltaX, float deltaY)
	{
		if (listener != null)
		{
			listener.scrolledBy(this, deltaX, deltaY);
		}
	}

	public class Size
	{
		float width;
		float height;
	}
}

