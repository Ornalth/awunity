using System;
using System.Collections.Generic;



public class InfluenceComputerV2 : Computer
{
	public InfluenceComputerV2(int team, GenericCO co) : base(team, co)
	{
		// TODO Auto-generated constructor stub
	}

	private const int BUILDING_INFLUENCE_RADIUS = 3;
	private const double INFLUENCE_DECAY_RATE = 0.8;
	protected internal const int LOW_GAS = 20;
	protected internal const double LOW_HEALTH = 20;
	protected internal const int DO_NOT_MOVE_SCORE_THRESHOLD = -20;
	protected internal const int CAPTURE_SCORE = 200;
	protected internal const int NO_MOVE_SCORE = -999;
	protected internal const int INFLUENCE_TENSION = 0;
	private const int INFLUENCE_FRIENDLY = 10;
	private const int INFLUENCE_HOSTILE = -10;

	//protected static final int MOVES_FILTERED_LEFT = 10;

	protected internal enum InfluenceType
	{
		HOSTILE,
		FRIENDLY,
		TENSION
	}

	private Position getBestTarget(Unit unit, int[,] influenceMap, List<Position> possibleTargets, int[] score, bool isPrimary)
	{
		Board board = Board.getInstance();
		int best = -999;
		Position bestTarget = null;
		Weapon weapon = unit.getWeapon(isPrimary);
		foreach (Position target in possibleTargets)
		{
			List<Position> damagedPositions = new List<Position>();

			damagedPositions.AddRange(Position.getAllPositionsUpTo(target, 0, weapon.radius));

			int targetScore = 5;

			// Incase radius
			foreach (Position dmgTarget in damagedPositions)
			{

				Unit targettedUnit = board.getTile(dmgTarget).getUnit();

				// Score for damage.
				if (targettedUnit != null)
				{
					int multiplier = targettedUnit.getTeam() == this.getTeam() ? - 1 : 1; // Friendly fire multiplier
					int estDmg = CombatCoordinator.getDamage(unit, isPrimary, targettedUnit);
					int cost = UnitFactory.getCost(targettedUnit);
					int health = targettedUnit.getHealth();
					double percentDmg = (double) estDmg / (double) targettedUnit.getMaxHealth();

					targetScore += (int)(percentDmg * cost / Constants.GOLD_PER_BUILDING * multiplier);

					// Can kill target!
					if (estDmg - health < 0)
					{
						targetScore += 10 * multiplier;
					}
					else if (targettedUnit.canRetaliate(unit))
					{
						targetScore -= 10;
					}

					if (targettedUnit.canCapture())
					{
						targetScore += 5 * multiplier;
					}

					Tile tile = board.getTile(targettedUnit.getPosition());
					if (tile.isBuilding())
					{
						Building build = (Building) tile;
						if (build.getTeam() == team)
						{
							targetScore += 10 * multiplier;
							if (build.hasGenerateAbility())
							{
								targetScore += 30 * multiplier;
							}

							if (build is Headquarters && targettedUnit.canCapture() && targettedUnit.getTeam() != team)
							{
								targetScore += 50;
							}
						}

					}

				}

			}

			// Score for position/capture.

			if (best < targetScore)
			{
				best = targetScore;
				bestTarget = target;
			}
		}
		score[0] = best;
		return bestTarget;
	}

	/// 
	/// <param name="unit"> </param>
	/// <param name="influenceMap"> </param>
	/// <param name="possibleMoves">
	///            -- NOTE THIS INCLUDES THE ORIGINS POSITION AKA NO MOVE. </param>
	/// <param name="savedBestScore">
	/// @return </param>
	private Position getBestMove(Unit unit, int[,] influenceMap, List<Position> possibleMoves, int[] savedBestScore)
	{
		Board board = Board.getInstance();
		int bestScore = -9999999;
		Position bestMove = null;

		foreach (Position move in possibleMoves)
		{
			int score = 3; //Entice action
			Tile tile = board.getTile(move);
			if (tile.isBuilding())
			{
				Building build = (Building) tile;
				if (build.getTeam() != unit.getTeam())
				{
					score += 3;
					// Can cap == !!
					if (unit.canCapture())
					{
						score += 5;
						// In process of capping, DO NOT MOVE/GO THERE
						if (build.getHealth() < Constants.BUILDING_MAX_HEALTH)
						{
							score += 10;
						}
						if (build is Headquarters)
						{
							score += 10;
						}
						Position p = board.getOtherTeamHQPosition(unit.getTeam());
						int distance = Position.getDistance(tile.getPosition(), p);
						if (distance < 10)
						{
							score += (10 - distance) * 2;
						}

					}
					// Block their generation
					if (build.hasGenerateAbility() && build.getTeam() != Constants.TEAM_NEUTRAL)
					{
						score += 8;
					}
				}

				if (build.getTeam() == unit.getTeam())
				{
					// Want to heal our own damage units.
					if (unit.getHealth() < unit.getMaxHealth() * (1.0 - Constants.UNIT_REGEN_ON_BUIDING_PERCENTAGE))
					{
						score += 8;
					}
					else
					{
						score -= 2;
					}

					if (unit.usesGas() && unit.getGas() < LOW_GAS)
					{
						score += 5;
					}
					else if (!unit.getPrimaryWeapon().hasAmmo() || !unit.getSecondaryWeapon().hasAmmo())
					{
						score += 3;
					}
					else
					{
						score -= 2;
					}

					// Dont want to block our own generation
					if (build.hasGenerateAbility())
					{
						score -= 8;
					}
				}
			}

			// Penalize movement if low on gas
			if (unit.usesGas() && unit.getGas() < LOW_GAS)
			{
				score -= 5;
			}

			if (!(unit.getMovementType() == MovementType.AIR))
			{
				score += tile.getDefense() * 2;
			}

			InfluenceType type = getInfluenceType(influenceMap[move.row,move.col]);
			switch (type)
			{
				case InfluenceType.FRIENDLY:
				{
					if (mode == Strategy.AGGRESSIVE)
					{
						score -= 10;
					}
					else if (mode == Strategy.DEFENSIVE)
					{
						score += 5;
					}
					break;
				}
				case InfluenceType.HOSTILE:
				{
					if (mode == Strategy.AGGRESSIVE)
					{
						score += 5;
					}
					else if (mode == Strategy.DEFENSIVE)
					{
						score -= 10;
					}
					break;
				}
				case InfluenceType.TENSION:
				{
					if (mode == Strategy.AGGRESSIVE)
					{
						score += 10;
					}
					else if (mode == Strategy.DEFENSIVE)
					{
						score += 5;
					}
					break;
				}
				default:
					break;
			}
			if (unit.canCapture())
			{
				if (type == InfluenceType.HOSTILE)
				{
					score -= 3;
				}
				else if (type == InfluenceType.FRIENDLY)
				{
					score += 3;
				}
				else if (type == InfluenceType.TENSION)
				{
					score += 5;
				}
			}
			// RANDOMIZER!
			score += rand.Next(5);
			if (score > bestScore)
			{
				bestMove = move;
				bestScore = score;
			}

		}
		savedBestScore[0] = bestScore;
		return bestMove;
	}

	private List<Move> evaluatePossibleMoves(Unit unit, int[,] influenceMap, List<Position> possibleMoves)
	{
		List<Move> moves = new List<Move>();
		int[] score = new int[1];
		foreach (Position pos in possibleMoves)
		{
			Move move = new Move();
			List<Position> single = new List<Position>();
			single.Add(pos);
			getBestMove(unit, influenceMap, single, score);
			move.scoreMove = score[0];
			move.move = pos;
			moves.Add(move);
		}
		return moves;
	}

	private void evaluateResultingActions(Unit unit, int[,] influenceMap, List<Move> evaluatedMoves)
	{

		Board board = Board.getInstance();
		int[] score = new int[1];
		foreach (Move move in evaluatedMoves)
		{
			int scoreAttackPrim = NO_MOVE_SCORE;
			int scoreAttackSec = NO_MOVE_SCORE;
			int scoreCap = NO_MOVE_SCORE;
			Position bestAttackPrimary = null;
			Position bestAttackSecondary = null;

			Tile origin = board.getTile(move.move);

			if (unit.canCapture() && origin.isBuilding() && unit.getTeam() != ((Building) origin).getTeam())
			{
				scoreCap = CAPTURE_SCORE;
			}

			if (unit.getPrimaryWeapon().hasAmmo())
			{
				List<Position> possibleTargets = board.getSimulatedTargets(unit, origin, true);
				if (possibleTargets.Count > 0)
				{
					bestAttackPrimary = getBestTarget(unit, influenceMap, possibleTargets, score, true);
					scoreAttackPrim = score[0];
				}
			}

			if (unit.getSecondaryWeapon().hasAmmo())
			{
				List<Position> possibleTargets = board.getTargets(origin, false);
				if (possibleTargets.Count > 0)
				{
					bestAttackSecondary = getBestTarget(unit, influenceMap, possibleTargets, score, false);
					scoreAttackSec = score[0];
				}
			}

			int maxScore = max(scoreAttackPrim, scoreAttackSec, scoreCap);

			if (maxScore == NO_MOVE_SCORE)
			{
				move.isCap = false;
				move.scoreAction = -5; // Bad if no action!
				move.attack = null;
				move.isPrimary = true;
			}
			else if (maxScore == scoreCap)
			{
				move.isCap = true;
				move.attack = null;
				move.scoreAction = scoreCap;
			}
			else if (maxScore == scoreAttackPrim)
			{
				move.isPrimary = true;
				move.attack = bestAttackPrimary;
				move.scoreAction = scoreAttackPrim;
				move.isCap = false;
			}
			else
			{
				move.isPrimary = false;
				move.attack = bestAttackSecondary;
				move.scoreAction = scoreAttackSec;
				move.isCap = false;
			}
		}

	}

	public override void makeMove()
	{
		// Currently random;
		Board board = Board.getInstance();

		int[,] influenceMap = calcInfluence();
		outputInfluence(influenceMap);
		board.sortUnitsByDistanceFromHq(team);
		board.sortUnitsByBeginAbleToCap(team);
		while (true)
		{
			Unit unit = board.getNextActiveUnit(team);
			if (unit == null)
			{
				break;
			}

			Tile origin = board.getTile(unit);


			if (unit.canMoveAndAttack())
			{

				List<Position> possibleMoves = board.getPossibleMoves(board.getTile(unit));
				possibleMoves.Add(unit.getPosition());

				// int score[] = new int[1];
				// bestMove = getBestMove(unit, influenceMap, possibleMoves,
				// score);
				List<Move> evaluatedMoves = evaluatePossibleMoves(unit, influenceMap, possibleMoves);
				// evaluatedMoves = filterMovesDownTo(evaluatedMoves,
				// MOVES_FILTERED_LEFT);

				evaluateResultingActions(unit, influenceMap, evaluatedMoves);

				// boolean canCapOrigin = unit.canCapture() &&
				// origin.isBuilding()
				// && ((Building) origin).getTeam() != unit.getTeam();
				Move theBestMove = findBestMove(evaluatedMoves);
				executeBestMove(unit, theBestMove);

			}
			else
			{
				int scoreAttackPrim = -999;
				int scoreAttackSec = -999;
				int scoreMoveOnly = -999;
				Position bestMove = null;
				Position bestAttackPrimary = null;
				Position bestAttackSecondary = null;

				List<Position> possibleMoves = board.getPossibleMoves(board.getTile(unit));
				possibleMoves.Add(unit.getPosition());
				int[] score = new int[1];
				bestMove = getBestMove(unit, influenceMap, possibleMoves, score);
				scoreMoveOnly = score[0];

				if (unit.getPrimaryWeapon().hasAmmo())
				{
					List<Position> possibleTargets = board.getTargets(origin, true);
					if (possibleTargets.Count > 0)
					{
						bestAttackPrimary = getBestTarget(unit, influenceMap, possibleTargets, score, true);
						scoreAttackPrim = score[0];
					}
				}

				if (unit.getSecondaryWeapon().hasAmmo())
				{
					List<Position> possibleTargets = board.getTargets(origin, false);
					if (possibleTargets.Count > 0)
					{
						bestAttackSecondary = getBestTarget(unit, influenceMap, possibleTargets, score, false);
						scoreAttackSec = score[0];
					}
				}

				int captureScore = -999;
				if (unit.canCapture() && origin.isBuilding() && ((Building) origin).getTeam() != unit.getTeam())
				{
					captureScore = CAPTURE_SCORE;
				}

				int best = max(scoreAttackPrim, scoreAttackSec, scoreMoveOnly, captureScore);
				if (best > DO_NOT_MOVE_SCORE_THRESHOLD)
				{
					if (best == scoreAttackPrim)
					{
						board.attack(origin, board.getTile(bestAttackPrimary), true);
					}
					else if (best == scoreAttackSec)
					{
						board.attack(origin, board.getTile(bestAttackSecondary), false);
					}
					else if (best == captureScore)
					{
						board.cap(origin);
					}
					else if (best == scoreMoveOnly)
					{
						board.move(origin, board.getTile(bestMove));
					}
				}
				board.stopUnit(unit);

			}
		}

		//Build algorithm
		List<Building> buildings = board.getBuildingsForTeam(team);

		//We want to build from highly contested buildings first.
		buildings.Sort(new ComparatorAnonymousInnerClassHelper(this, influenceMap));
		List<Unit> allyUnits = board.getUnitsForTeam(team);
		List<Unit> enemyUnits = board.getUnitsForTeam(team ^ 1);
		int numAllyUnits = allyUnits.Count;
		int numEnemyUnits = enemyUnits.Count;
		foreach (Building build in buildings)
		{
			if (build.canGenerate())
			{
				List<Unit> canBuy = new List<Unit>();
				List<Unit> canGen = build.getGenerateOptions();
				foreach (Unit unit in canGen)
				{
					if (UnitFactory.getCost(unit) < gold)
					{
						canBuy.Add(unit);
					}
				}

				// ??? buy random unit atm;
				// if (canBuy.size() > 0)
				// board.purchase(canBuy.get(rand.nextInt(canBuy.size())),
				// build);
				if (canBuy.Count > 0)
				{
					Position pos = build.getPosition();
					int influence = influenceMap[pos.row,pos.col];
					MyLogger.log("Tension at building :" + influence);

					//buy largest

					int best = -2;
					Unit bestUnit = null;
					if (numEnemyUnits > numAllyUnits + 5)
					{
						best = 999;
						foreach (Unit unit in canBuy)
						{
							int unitStr = calculateUnitStrength(unit);
							//No overkill.
							if (unitStr < 4 && best > unitStr)
							{
								best = unitStr;
								bestUnit = unit;
								numAllyUnits++;
							}
						}

					}
					else if (influence < 0) // Dont want to buy ranged units when hgih tension
					{
						foreach (Unit unit in canBuy)
						{
							int unitStr = calculateUnitStrength(unit);
							int result = unitStr + influence;
							//No overkill.
							if (unit.canMoveAndAttack() && result > best && result > 0)
							{
								best = result;
								bestUnit = unit;
							}
						}
					}
					else if (influence < 5) // Dont want to buy ranged units when hgih tension
					{
						foreach (Unit unit in canBuy)
						{
							int unitStr = calculateUnitStrength(unit);
							int result = unitStr + influence;
							//No overkill.
							if (unit.canMoveAndAttack() && result > best && result < 5)
							{
								best = result;
								bestUnit = unit;
							}
						}
					}
					else if (influence < 15) // buy ranged/cap
					{
						foreach (Unit unit in canBuy)
						{
							int unitStr = calculateUnitStrength(unit);
							int result = unitStr + influence;
							if (unit.canCapture() || unit.canMoveAndAttack())
							{
								bestUnit = unit;
								best = result;
							}
							else if (result < best && bestUnit != null && bestUnit.canCapture())
							{
								bestUnit = unit;
								best = result;
							}
						}
					}
					else if (influence < 25) // buy cap
					{
						foreach (Unit unit in canBuy)
						{
							int unitStr = calculateUnitStrength(unit);
							int result = unitStr + influence;
							if (unit.canCapture())
							{
								bestUnit = unit;
								best = result;
							}
							else if (result < best && bestUnit != null && bestUnit.canCapture())
							{
								bestUnit = unit;
								best = result;
							}
						}
					}


					if (bestUnit != null)
					{
						board.purchase(bestUnit, build);
					}
				}
			}
		}

		board.endTurn();
	}

	private class ComparatorAnonymousInnerClassHelper : IComparer<Building>
	{
		private readonly InfluenceComputerV2 outerInstance;

		private int[,] influenceMap;

		public ComparatorAnonymousInnerClassHelper(InfluenceComputerV2 outerInstance, int[,] influenceMap)
		{
			this.outerInstance = outerInstance;
			this.influenceMap = influenceMap;
		}


		public virtual int Compare(Building b1, Building b2)
		{
			Position pb1 = b1.getPosition();
			Position pb2 = b2.getPosition();
			int influence1 = influenceMap[pb1.row,pb1.col];
			int influence2 = influenceMap[pb2.row,pb2.col];
			if (influence1 > influence2)
			{
				return -1;
			}
			else if (influence1 == influence2)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}

	}

	private int calculateUnitStrength(Unit unit)
	{

		return UnitFactory.getCost(unit) / 10;
	}

	private void executeBestMove(Unit unit, Move theBestMove)
	{
		MyLogger.log("Unit at " + unit.getPosition());
		Board board = Board.getInstance();
		if (theBestMove == null)
		{
			board.stopUnit(unit);
			return;
		}

		Tile origin = board.getTile(unit.getPosition());

		if (theBestMove.move != null)
		{
			board.move(origin, board.getTile(theBestMove.move));

		}

		// Possible movement.
		origin = board.getTile(unit.getPosition());
		if (theBestMove.attack != null)
		{
			board.attack(origin, board.getTile(theBestMove.attack), theBestMove.isPrimary);
		}

		if (theBestMove.isCap)
		{
			board.cap(origin);

		}
		board.stopUnit(unit);
	}

	private int max(params int[] scores)
	{
		if (scores.Length == 0)
		{
			return -99999999;
		}
		else
		{
			int best = scores[0];
			for (int i = 1; i < scores.Length; i++)
			{
				if (best < scores[i])
				{
					best = scores[i];
				}
			}
			return best;
		}
	}

	/// <summary>
	/// CALC INFLUENCE
	/// </summary>

	private int[,] calcInfluence()
	{
		Board board = Board.getInstance();
		int[,] allyInfluence = new int[Constants.height,Constants.width];
		int[,] enemyInfluence = new int[Constants.height,Constants.width];
		int[,] totalInfluence = new int[Constants.height,Constants.width];
		int enemyGold = board.getPlayer(team ^ 1).getGold();
		int allyGold = board.getPlayer(team).getGold();
		List<Unit> enemies = board.getUnitsForTeam(team ^ 1); // todo
		// fix
		List<Unit> allies = board.getUnitsForTeam(team);
		List<Building> enemyBuildings = board.getBuildingsForTeam(team ^ 1);
		List<Building> allyBuildings = board.getBuildingsForTeam(team);

		foreach (Unit enemy in enemies)
		{
			getInfluence(enemyInfluence, enemy);
		}
/*
		for (Building building : enemyBuildings)
		{
			getInfluence(enemyInfluence, building, enemyGold);
		}
*/
		foreach (Unit ally in allies)
		{
			getInfluence(allyInfluence, ally);
		}
/*
		for (Building building : allyBuildings)
		{
			getInfluence(allyInfluence, building, allyGold);
		}
*/
		for (int row = 0; row < Constants.height; row++)
		{
			for (int col = 0; col < Constants.width; col++)
			{
				totalInfluence[row,col] = allyInfluence[row,col] - enemyInfluence[row,col];
			}
		}

		//long endTime = DateTimeHelperClass.CurrentUnixTimeMillis();
	//	MyLogger.log("Total execution time for influence: " + (endTime - startTime));
		return totalInfluence;

	}

	// TODO CHANGE BASED ON WHAT KIND OF UNITS OPPS HAVE AND WHAT I HAVE (EX.
	// alotta air? go towards nonAIR)
	private void getInfluence(int[,] influenceMap, Building building, int gold)
	{
		Position origin = building.getPosition();
		int influence;
		if (building is Airport)
		{
			influence = 50;
			for (int distance = 0; distance <= BUILDING_INFLUENCE_RADIUS; distance++)
			{
				foreach (Position pos in Position.getAllPositionsXSquaresAway(origin, distance))
				{
					influenceMap[pos.row,pos.col] += (int)(influence * Math.Pow(INFLUENCE_DECAY_RATE, distance));
				}
			}
		}
		if (building is Barracks)
		{
			influence = 40;

			for (int distance = 0; distance <= BUILDING_INFLUENCE_RADIUS; distance++)
			{
				foreach (Position pos in Position.getAllPositionsXSquaresAway(origin, distance))
				{
					influenceMap[pos.row,pos.col] += (int)(influence * Math.Pow(INFLUENCE_DECAY_RATE, distance));
				}
			}
		}
		if (building is Seaport)
		{
			influence = 20;

			for (int distance = 0; distance <= BUILDING_INFLUENCE_RADIUS; distance++)
			{
				foreach (Position pos in Position.getAllPositionsXSquaresAway(origin, distance))
				{
					influenceMap[pos.row,pos.col] += (int)(influence * Math.Pow(INFLUENCE_DECAY_RATE, distance));
				}
			}
		}

		if (building is City)
		{
			influence = 10;

			for (int distance = 0; distance <= BUILDING_INFLUENCE_RADIUS; distance++)
			{
				foreach (Position pos in Position.getAllPositionsXSquaresAway(origin, distance))
				{
					influenceMap[pos.row,pos.col] += (int)(influence * Math.Pow(INFLUENCE_DECAY_RATE, distance));
				}
			}
		}

		if (building is Headquarters)
		{
			influence = 75;

			for (int distance = 0; distance <= BUILDING_INFLUENCE_RADIUS; distance++)
			{
				foreach (Position pos in Position.getAllPositionsXSquaresAway(origin, distance))
				{
					influenceMap[pos.row,pos.col] += (int)(influence * Math.Pow(INFLUENCE_DECAY_RATE, distance));
				}
			}
		}
	}

	// TODO add in damage. && self square.
	private void getInfluence(int[,] influenceMap, Unit unit)
	{

		int sqInfluence = unit.getHealth();
		int numMoves = unit.getMoves();

		Position origin = unit.getPosition();
		bool canCap = unit.canCapture();
		double percentHealth = unit.getHealth() / unit.getMaxHealth();
		int influence = (int)(percentHealth * UnitFactory.getCost(unit)) / 10;
		foreach (Position pos in Position.getAllPositionsUpTo(origin, 0, unit.getPrimaryWeapon().maxRange + unit.getMoves()))
		{
			influenceMap[pos.row,pos.col] += (int)(influence * Math.Pow(INFLUENCE_DECAY_RATE, Position.getDistance(origin, pos)));
		}
		// also if can retal!

				// Influence from weapon/damage
	/*
	Weapon primary = unit.getPrimaryWeapon();
	int dmgInfluence = primary.hasInfiniteAmmo() ? primary.baseDamage * 5
			: primary.baseDamage * primary.ammo / 3;
	dmgInfluence /= 10;
	Weapon secondary = unit.getSecondaryWeapon();
	int dmgInfluence2 = secondary.hasInfiniteAmmo() ? secondary.baseDamage * 5
			: secondary.baseDamage * secondary.ammo / 3;
	dmgInfluence2 /= 10;

	if (dmgInfluence > 0)
	{
		for (int distance = primary.minRange; distance <= primary.maxRange; distance++)
		{
			for (Position pos : Position.getAllPositionsXSquaresAway(
					origin, distance))
			{
				influenceMap[pos.row,pos.col] += dmgInfluence;
			}
		}

		if (unit.canMoveAndAttack())
		{
			for (int distance = primary.maxRange; distance <= primary.maxRange
					+ numMoves; distance++)
			{
				for (Position pos : Position.getAllPositionsXSquaresAway(
						origin, distance))
				{
					// TODO better factor.
					influenceMap[pos.row,pos.col] += dmgInfluence
							* Math.pow(INFLUENCE_DECAY_RATE,
									(distance - primary.maxRange));
				}
			}
		}

	}

	if (dmgInfluence2 > 0)
	{
		for (int distance = primary.minRange; distance <= primary.maxRange; distance++)
		{
			for (Position pos : Position.getAllPositionsXSquaresAway(
					origin, distance))
			{
				influenceMap[pos.row,pos.col] += dmgInfluence2;
			}
		}

		if (unit.canMoveAndAttack())
		{
			for (int distance = secondary.maxRange; distance <= secondary.maxRange
					+ numMoves; distance++)
			{
				for (Position pos : Position.getAllPositionsXSquaresAway(
						origin, distance))
				{
					influenceMap[pos.row,pos.col] += dmgInfluence2
							* Math.pow(INFLUENCE_DECAY_RATE,
									(distance - secondary.maxRange));
				}
			}
		}
	}
	*/
	}

	private InfluenceType getInfluenceType(int i)
	{
		if (i <= INFLUENCE_HOSTILE)
		{
			return InfluenceType.HOSTILE;
		}

		if (i > INFLUENCE_HOSTILE && i < INFLUENCE_FRIENDLY)
		{
			return InfluenceType.TENSION;
		}

		return InfluenceType.FRIENDLY;
	}

	private Move findBestMove(List<Move> moves)
	{
		int bestScore = -99999;
		Move bestMove = null;
		// if (moves.size() == 0)
		// return new Move();

		double moveMultiplier = 1;
		double actionMultiplier = 1;
		switch (mode)
		{
			case Strategy.AGGRESSIVE:
				moveMultiplier = 0.75;
				actionMultiplier = 1.25;
				break;
			case Strategy.BALANCED:
				moveMultiplier = 1.25;
				actionMultiplier = 0.75;
				break;
			case Strategy.DEFENSIVE:
				moveMultiplier = 0.75;
				actionMultiplier = 1.25;
				break;
			default:
				break;

		}
		foreach (Move move in moves)
		{
			double score = move.scoreMove * moveMultiplier + move.scoreAction * actionMultiplier;

			if (bestScore < score)
			{
				bestScore = (int) score;
				bestMove = move;
			}
		}

		if (bestMove.scoreAction + bestMove.scoreMove < DO_NOT_MOVE_SCORE_THRESHOLD)
		{
			return null;
		}
		return bestMove;
	}

	private List<Move> filterMovesDownTo(List<Move> moves, int left)
	{
		if (moves.Count <= left)
		{
			return moves;
		}

		moves.Sort(Move.SORT_BY_MOVE_SCORE_DESC);
		List<Move> newMoves = moves.GetRange(0, left);
		return newMoves;

	}

	private void outputInfluence(int[,] influenceMap)
	{
		for (int row = 0; row < Constants.height; row++)
		{
			for (int col = 0; col < Constants.width; col++)
			{
				MyLogger.log(" " + influenceMap[row,col]);
			}
			MyLogger.log("");
		}
	}
}
