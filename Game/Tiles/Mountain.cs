﻿
public class Mountain : Tile
{
	protected internal Mountain(int row, int col) : base(row, col)
	{
	}

	public override string ToString()
	{
		return "MTN";
	}

	protected internal override int getFootUnitCost()
	{
		return 2;
	}

	protected internal override int getTireUnitCost()
	{
		return 4;
	}

	protected internal override int getTreadUnitCost()
	{
		return 9999;
	}

	protected internal override int getSeaUnitCost()
	{
		return 9999;
	}

	protected internal override int getAirUnitCost()
	{
		return 1;
	}

	public override int getDefense()
	{
		return 4;
	}
}
