﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GenericStatusDetailsViewScript : MonoBehaviour {

	public interface GenericStatusDetailsListener 
	{
		void endTurnRequest(GenericStatusDetailsViewScript scr);
		void regularAbilityRequest(GenericStatusDetailsViewScript scr);
		void superAbilityRequest(GenericStatusDetailsViewScript scr);
		void undoMoveRequest(GenericStatusDetailsViewScript src);
	}

	Text goldText;
	Text turnText;
	Text abilityPointsText;
	Text regularAbilityButtonText;
	Text superAbilityButtonText;
	Text undoMoveButtonText;

	Button endTurnButton;
	Button regularAbilityButton;
	Button superAbilityButton;
	Button undoMoveButton;

	List<GenericStatusDetailsListener> listeners = new List<GenericStatusDetailsListener>();


	void Awake()
	{
		Text[] texts = GetComponentsInChildren<Text>();
		foreach (Text text in texts)
		{
			if (text.name.Equals("Gold"))
			{
				goldText = text;
			}
			else if (text.name.Equals("Turn"))
			{
				turnText = text;
			}
			else if (text.name.Equals("AbilityPointsText"))
			{
				abilityPointsText = text;
			}
			else if (text.name.Equals("RegularAbilityButtonText"))
			{
				regularAbilityButtonText = text;
			}
			else if (text.name.Equals("SuperAbilityButtonText"))
			{
				superAbilityButtonText = text;
			}
			else if (text.name.Equals("UndoMoveButtonText"))
			{
				undoMoveButtonText = text;
			}
		}

		Button[] buttons = GetComponentsInChildren<Button>();
		foreach (Button button in buttons)
		{
			if (button.name.Equals("EndTurnButton"))
			{
				endTurnButton = button;
				endTurnButton.onClick.AddListener(endTurnClicked);
			}
			else if (button.name.Equals("RegularAbilityButton"))
			{
				regularAbilityButton = button;
				regularAbilityButton.onClick.AddListener(regularAbilityButtonClicked);
			}
			else if (button.name.Equals("SuperAbilityButton"))
			{
				superAbilityButton = button;
				superAbilityButton.onClick.AddListener(superAbilityButtonClicked);
			}
			else if (button.name.Equals("Undo"))
			{
				undoMoveButton = button;
				undoMoveButton.onClick.AddListener(undoMoveButtonClicked);
			}
		}
	}

	public void subscribe(GenericStatusDetailsListener listener)
	{
		listeners.Add(listener);
	}

	public void endTurnClicked()
	{
		foreach (GenericStatusDetailsListener listener in listeners)
		{
			listener.endTurnRequest(this);
		}
	}

	public void regularAbilityButtonClicked()
	{
		foreach (GenericStatusDetailsListener listener in listeners)
		{
			listener.regularAbilityRequest(this);
		}
	}

	public void superAbilityButtonClicked()
	{
		foreach (GenericStatusDetailsListener listener in listeners)
		{
			listener.superAbilityRequest(this);
		}
	}

	public void undoMoveButtonClicked()
	{
		foreach (GenericStatusDetailsListener listener in listeners)
		{
			listener.undoMoveRequest(this);
		}
	}

	public void update(Board b)
	{
		Player player = b.getCurrentPlayer();
		goldText.text = "" + player.getGold() + " gold";
		turnText.text = "Day " + b.getDay();
		abilityPointsText.text = "" + player.getCurrentAbilityPoints() + " / " + player.getMaxAbilityPoints() + " pts.";

		regularAbilityButton.interactable = b.canActivateRegularAbility();
		superAbilityButton.interactable = b.canActivateSuperAbility();
	}
}