﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InputTextBox : MonoBehaviour {

	IInputTextBoxController controller;
	public interface IInputTextBoxController
	{
		void valueEntered(InputTextBox tb,int value);
		void cancelledInput(InputTextBox tb);
	}

	Text titleText;
	Text text;
	//StringBuilder builder;
	//Scrollbar scrollbar;
	CanvasGroup canvasGroup;
	public InputField textInputField;


	int min;
	int max;

	void Awake()
	{
		canvasGroup = gameObject.GetComponentInChildren<CanvasGroup>();
		text = gameObject.GetComponentInChildren<Text>();
		text.text = "";
	}

	public void setValues(string titleTextMsg, int min, int max)
	{
		titleText.text = titleTextMsg;
		text.text = "";
		this.min = min;
		this.max = max;
	}

	public void setController(IInputTextBoxController cc)
	{
		controller = cc;
	}
	
	public void Update()
	{
	    if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown("enter") 
	     	&& EventSystem.current.currentSelectedGameObject == textInputField.gameObject)
	    {
	    	int ans = 0;
	    	ViewHelper.parseInt(textInputField.text, out ans);
			if (ans >= min && ans <= max)
			{
				//TELL PARENT
				controller.valueEntered(this, ans);
			}
	       	else
	       	{
	       		//ERROR
	       	}
	    }
	    else if (false)//TODO!!!) //CANCEL
	    {
	    	controller.cancelledInput(this);
	    }
	}

	public CanvasGroup getCanvasGroup()
	{
		return canvasGroup;
	}
}
