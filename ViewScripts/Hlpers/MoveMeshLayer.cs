﻿using UnityEngine;
using System.Collections;

public class MoveMeshLayer : MonoBehaviour {

	public string layerName = null;

	void Start ()
	{

		this.GetComponent<Renderer>().sortingLayerID = this.transform.parent.GetComponent<Renderer>().sortingLayerID;
		this.GetComponent<Renderer>().sortingOrder = this.transform.parent.GetComponent<Renderer>().sortingOrder + 5;
		if (layerName != null)
		{
			this.GetComponent<Renderer>().sortingLayerName = layerName;
		}
	}

	public void setSortingLayerID(int id)
	{
		this.GetComponent<Renderer>().sortingLayerID = id;
		this.GetComponent<Renderer>().sortingOrder = this.transform.parent.GetComponent<Renderer>().sortingOrder + 5;
	}
}
