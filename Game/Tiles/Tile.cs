using System;


public abstract class Tile : IDrawable
{
	internal Unit unit;
	public int row;
	public int col;

	protected internal Tile(int row, int col)
	{
		this.row = row;
		this.col = col;
	}

	public virtual bool isBuilding()
	{
		return false;
	}

	public virtual int getMovementCost(Unit movingUnit)
	{
		if (movingUnit == null)
		{
			MyLogger.log("ERROR IN GETMOVEMENTCOST MOVING UNIT IS NULL");
			return 99999;
		}

		if (unit != null && movingUnit.getTeam() != unit.getTeam())
		{
			return 99999;
		}
		MovementType moveType = movingUnit.getMovementType();

		if (moveType == MovementType.AIR)
		{
			return getAirUnitCost();
		}
		else if (moveType == MovementType.SEA)
		{
			return getSeaUnitCost();

		}
		else if (moveType == MovementType.TREADS)
		{
			return getTreadUnitCost();
		}
		else if (moveType == MovementType.TIRE)
		{
			return getTireUnitCost();

		}
		else if (moveType == MovementType.FOOT)
		{
			return getFootUnitCost();
		}

		MyLogger.log("ERROR in get cost");
		return 99999;
	}

	protected internal abstract int getFootUnitCost();
	protected internal abstract int getTireUnitCost();
	protected internal abstract int getTreadUnitCost();
	protected internal abstract int getSeaUnitCost();
	protected internal abstract int getAirUnitCost();

	public Unit getUnit()
	{
		return unit;
	}

	public void setUnit(Unit unit)
	{
		this.unit = unit;
	}
	public abstract int getDefense();

	public virtual string getDrawableName()
	{
		return TileFactory.getDrawableName(this);

	}

	public Position getPosition()
	{
		return new Position(row, col);
	}

	public virtual AttackObject attack(Tile tile, bool isPrimary)
	{
		//Should not happen.
		if (unit == null)
		{
			return null;
		}
		return unit.attack(this.getPosition(), tile,isPrimary);
	}



	public virtual bool hasUnit()
	{
		return unit != null;
	}


	public virtual bool canRetaliate(Unit atker)
	{
		if (!hasUnit())
		{
			return false;
		}

		return unit.canRetaliate(atker);
	}

	//TODO much later.
	//Tile based Attack
	/*
	public AttackObject attack(Tile tile)
	{

	}*/
}
