using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BuildingViewScript : MonoBehaviour, BuildOptionScript.BuildOptionListener{


	public interface BuildingViewListener
	{
		void buildOptionSelected(BuildingViewScript scr, BuildOptionDetail details);
		void buildOptionCancelled(BuildingViewScript scr);
	}



	public GameObject buildilngOptionPrefab;
	public GameObject contentPanel;
	RectTransform contentPanelTransform;

	List<BuildingViewListener> listeners = new List<BuildingViewListener>();
	List<GameObject> buildingObjs = new List<GameObject>();
	List<BuildOptionScript> viewScripts;
	Button cancelButton;
	Text goldText;

	private const int numViews = 20;

	void Awake()
	{
		viewScripts = new List<BuildOptionScript>();

		for (int i = 0; i < numViews; i++)
		{
			GameObject obj = Instantiate (buildilngOptionPrefab) as GameObject;
			BuildOptionScript optionScr = obj.GetComponent<BuildOptionScript>();
			//optionScr.setName("BKRAGH " + i);
			optionScr.subscribe(this);
			obj.transform.SetParent(contentPanel.transform, false);
			viewScripts.Add(optionScr);
			buildingObjs.Add(obj);
		}
		


		Button[] buttons = GetComponentsInChildren<Button>();
		foreach (Button button in buttons)
		{
			if (button.name.Equals("CancelButton"))
			{
				cancelButton = button;
				cancelButton.onClick.AddListener(cancelButtonClicked);
			}
		}

		Text[] texts = GetComponentsInChildren<Text>();
		foreach (Text text in texts)
		{
			if (text.name.Equals("CurrentGold"))
			{
				goldText = text;
			}
		}

		contentPanelTransform = contentPanel.GetComponent<RectTransform>();
	}

	public void buildOptionSelected(BuildOptionScript scr)
	{
		int index = viewScripts.IndexOf(scr);
		Debug.Log("OPTION SELECTED! " + index);
		BuildOptionDetail details = scr.getDetails();
		//BuildOptionDetail
		foreach (BuildingViewListener listener in listeners)
		{
			listener.buildOptionSelected(this, details);
		}
	}

	public void cancelButtonClicked()
	{
		foreach (BuildingViewListener listener in listeners)
		{
			listener.buildOptionCancelled(this);
		}
	}

	public void subscribe(BuildingViewListener scr)
	{
		listeners.Add(scr);
	}

	public void setData(Building build, List<Unit> units, int gold,
			int team)
	{
		int index = 0;

		foreach (Unit unit in units)
		{
			BuildOptionScript scr = viewScripts[index];
			BuildOptionDetail detail = new BuildOptionDetail();
			int costAmt = UnitFactory.getCost(unit);

				
			detail.canBuild = gold >= costAmt;
			detail.unit = unit;
			detail.team = team;
			scr.setDetails(detail);
			ViewHelper.show(buildingObjs[index]);
			++index;
		}

		for (; index < numViews; ++index)
		{
			ViewHelper.hide(buildingObjs[index]);
		}

		goldText.text = "" + gold + " Rez";

		//Attempting to move to top of thing, however doesnt work :|... oh well -999 hack improvise np.
		Vector2 size = contentPanelTransform.sizeDelta;
		contentPanelTransform.anchoredPosition = new Vector2(contentPanelTransform.anchoredPosition.x, -999);// - size.y / 2);

	}
}
