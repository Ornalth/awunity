﻿
//TODO maybe do weapon types?

public interface ICombatModifierEffect
{
	double getAirWeaponDamageModifier();
	double getGroundWeaponDamageModifier();

	double getSeaWeaponDamageModifier();
	double getAirWeaponDefenseModifier();

	double getGroundWeaponDefenseModifier();

	double getSeaWeaponDefenseModifier();
}
