﻿
public class Grass : Tile
{
	protected internal Grass(int row, int col) : base(row, col)
	{
	}

	public override string ToString()
	{
		return "GRA";
	}

	protected internal override int getFootUnitCost()
	{
		return 1;
	}

	protected internal override int getTireUnitCost()
	{
		return 2;
	}

	protected internal override int getTreadUnitCost()
	{
		return 1;
	}

	protected internal override int getSeaUnitCost()
	{
		return 9999;
	}

	protected internal override int getAirUnitCost()
	{
		return 1;
	}

	public override int getDefense()
	{
		return 1;
	}
}