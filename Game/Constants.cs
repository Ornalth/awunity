

public class Constants
{
	public const int width = 15;
	public const int height = 15;
	
	public const string fullRootPath = "Assets/Resources/Code/Game/Data/";
	public const string resourcesRootPath = "Code/Game/Data/";
	public const string boardFile = fullRootPath + "board.txt";
	public const string damageChartFile = fullRootPath + "damage.txt";
	public const string unitFile = resourcesRootPath + "unit";//.json";
	public const int TEAM_RED = 1;
	public const int TEAM_BLUE = 0;
	public const int TEAM_NEUTRAL = 2;
	public const int STARTING_GOLD = 50;
	public const int GOLD_PER_BUILDING = 10;
	public const int BUILDING_MAX_HEALTH = 100; // TODO
	public const double UNIT_REGEN_ON_BUIDING_PERCENTAGE = 0.2;
	public const double REPAIR_COST_FACTOR = UNIT_REGEN_ON_BUIDING_PERCENTAGE;
	public const int BUILDING_HEALTH_REGEN = BUILDING_MAX_HEALTH;
	//public static final int TILEVIEW_SIZE = 60;
	//public static final int UNIT_MAX_HEALTH = 100;
	public const int LUCK_MIN = -3;
	public const int LUCK_MAX = 3;
	public const int TILE_WIDTH_SIZE = 40;
	public const int TILE_HEIGHT_SIZE = 40;
	public const int INFINITE = 99999;


	public const bool SHOULD_LOG = true;
}
