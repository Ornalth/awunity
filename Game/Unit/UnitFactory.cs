﻿using System;
using System.Collections.Generic;
using System.Collections;
using LitJson;
using UnityEngine;

/*
         {
            "ID": 3,
            "class": "SupplyTruck",
            "name": "Supply Truck",
            "health": 40,
            "cost": 80,
            "move": 6,
            "moveType": "TREAD",
            "MAT": true,
            "capture": false,
            "gas": 50,
            "upkeepGas": 1,
            "built": "BARRACKS"
        },
 */
public class UnitFactory
{
	private static Dictionary<int, Unit> allUnits = new Dictionary<int, Unit>();
	private static List<Unit> seaGenUnits = new List<Unit>();
	private static List<Unit> barGenUnits = new List<Unit>();
	private static List<Unit> airGenUnits = new List<Unit>();
	private static Dictionary<int, int> costMap = new Dictionary<int, int>();

	public static void initUnitsFromFile()
	{
		allUnits = new Dictionary<int, Unit>();
		seaGenUnits = new List<Unit>();
		barGenUnits = new List<Unit>();
		airGenUnits = new List<Unit>();
		costMap = new Dictionary<int, int>();
		try
		{

			JsonData mainObj = getJsonFile();

			JsonData array = (JsonData) mainObj["units"];
			
			for (int unitNumber = 0; unitNumber < array.Count; unitNumber++)
			{
				JsonData unitObj = (JsonData) array[unitNumber];
				string clsName = (string)unitObj["class"];

				//Holy crap the hack... dayum.
				Type cls = Type.GetType(clsName);
				Unit unit = new Unit();//(Unit) Activator.CreateInstance(cls);
				unit.setName((string)unitObj["name"]);
				unit.setMaxHealth((int)unitObj["health"]);
				unit.setMaxMoves((int)unitObj["move"]);
				unit.setId((int)unitObj["ID"]);
				unit.setArmor(DamageType.typeFromString((string) unitObj["armor"]));
				unit.setMovementType(MovementType.movementTypeFromString((string) unitObj["movementType"]));
				unit.setDrawableName((string) unitObj["img"]);
				if (costMap.ContainsKey(unit.getId()))
				{
					MyLogger.log("ERROR NON UNIQUE IDS");
				}
				costMap[unit.getId()] = ((int)unitObj["cost"]);

				//Optionals

				if (JsonDataContainsKey(unitObj, "capture"))
				{
					unit.setCanCapture((bool) unitObj["capture"]);
				}
				if (JsonDataContainsKey(unitObj, "captureRate"))
				{
					unit.setCapSpeed((double) unitObj["captureRate"]);
				}

				if (JsonDataContainsKey(unitObj, "MAT"))
				{
					unit.setCanMoveAndAttack((bool) unitObj["MAT"]);
				}

				if (JsonDataContainsKey(unitObj, "gas"))
				{
					unit.setMaxGas(((int)unitObj["gas"]));
				}

				if (JsonDataContainsKey(unitObj, "gasUpkeep"))
				{
					unit.setGasUpkeep(((int)unitObj["gasUpkeep"]));
				}

				if (JsonDataContainsKey(unitObj, "dieNoGas"))
				{
					unit.setDieOnNoGas((bool) unitObj["dieNoGas"]);
				}



				if (JsonDataContainsKey(unitObj, "primary"))
				{
					JsonData weaponObj = (JsonData)unitObj["primary"];
					Weapon primary = parseWeapon(weaponObj);
					unit.setPrimaryWeapon(primary);
				}

				if (JsonDataContainsKey(unitObj, "secondary"))
				{
					JsonData weaponObj = (JsonData)unitObj["secondary"];
					Weapon secondary = parseWeapon(weaponObj);
					unit.setSecondaryWeapon(secondary);
				}

				if (JsonDataContainsKey(unitObj, "built"))
				{
					string builtFrom = (string)unitObj["built"];
					if (builtFrom.Equals("BARRACKS"))
					{
						barGenUnits.Add(unit);
					}
					else if (builtFrom.Equals("AIRPORT"))
					{
						airGenUnits.Add(unit);
					}
					else if (builtFrom.Equals("SEAPORT"))
					{
						seaGenUnits.Add(unit);
					}
					else
					{
						MyLogger.log("ERROR IN BUILT FROM PARSE!");
					}
				}
				allUnits.Add(unit.getId(), unit);
				/*WEAPON--
				String name;
				int radius = 0;
				int baseDamage = 5;
				WeaponType type = WeaponType.BULLETS;
				int minRange = 1;
				int maxRange = 1;
				int maxAmmo = Constants.INFINITE;
				int ammo = maxAmmo;
				 */


				//Optionals.

				//GAS
				//Upkeep gas
				//unit.setCanCapture(((int)unitObj["ID"]).intValue());
				//unit.setCanMAT((Boolean) unitObj.opt("MAT"))
			}
		}
		catch (Exception e)
		{
			MyLogger.log(e.ToString());
			MyLogger.log(e.StackTrace);
		}

		/*
		 * Attach cost onto this. will allow for less crazy crap.
	   int health;
		MoveType moveType;
		int team;
		int moves;
		public int row;
		public int col;
		boolean canMoveAndAttack;
		boolean hasMoved;
		boolean hasAttacked;
		Weapon primary;
		Weapon secondary;
		boolean canRetal
		Generate from what building!
		int kills = 0;
		 */

	}

	private static JsonData getJsonFile()
	{
		TextAsset textFile = (TextAsset)Resources.Load(Constants.unitFile, typeof(TextAsset));
		return JsonMapper.ToObject(textFile.text);
	}

	private static Weapon parseWeapon(JsonData weaponObj)
	{
		Weapon weapon = new Weapon();
		weapon.name = (string) weaponObj["name"];
		if (JsonDataContainsKey(weaponObj, "radius"))
		{
			weapon.radius = (((int)weaponObj["radius"]));
		}
		else
		{
			weapon.radius = 0;
		}

		weapon.baseDamage = (((int)weaponObj["base"]));
		weapon.type = DamageType.typeFromString((string) weaponObj["type"]);
		weapon.minRange = (((int)weaponObj["min"]));
		weapon.maxRange = (((int)weaponObj["max"]));
		if (JsonDataContainsKey(weaponObj, "ammo"))
		{
			weapon.maxAmmo = (((int)weaponObj["ammo"]));
			weapon.ammo = weapon.maxAmmo;
		}
		else
		{
			weapon.maxAmmo = Constants.INFINITE;
			weapon.ammo = weapon.maxAmmo;
		}

		if (JsonDataContainsKey(weaponObj, "retal"))
		{
			weapon.retal = (bool)weaponObj["retal"];
		}

		return weapon;
	}

	public static int getRepairCost(Unit unit)
	{
		return (int)(getCost(unit) * Constants.UNIT_REGEN_ON_BUIDING_PERCENTAGE);
	}

	// TODO remake this no char no row col or team.
	public static Unit createUnit(Unit u, int row, int col, int team)
	{
		int id = u.getId();
		Unit unit = allUnits[id];
		Unit newUnit = unit.deepCopy();
		newUnit.row = row;
		newUnit.col = col;
		newUnit.setTeam(team);
		newUnit.hAttacked = true;
		newUnit.hMoved = true;
		return newUnit;
	}

	public static List<Unit> generateList(Building building)
	{
		List<Unit> units = new List<Unit>();


		if (building is Barracks)
		{
			units = getBarracksUnits();
		}
		else if (building is Airport)
		{
			units =  getAirportUnits();
		}
		else if (building is Seaport)
		{
			units = getSeaportUnits();
		}

		foreach (Unit unit in units)
		{
			unit.setTeam(building.getTeam());
		}
		return units;
	}

	private static List<Unit> getSeaportUnits()
	{
		return new List<Unit>(seaGenUnits);
	}

	private static List<Unit> getAirportUnits()
	{
		return new List<Unit>(airGenUnits);
	}

	private static List<Unit> getBarracksUnits()
	{
		return new List<Unit>(barGenUnits);
	}

	public static bool canGenerate(Building building)
	{
		if (building is Barracks || building is Airport || building is Seaport)
		{
			return true;
		}
		return false;
	}

	public static int getCost(Unit unit)
	{
		int cost = 0;
		cost = costMap[unit.getId()];
		return cost;
	}

	static public  bool JsonDataContainsKey(JsonData data,string key)
    {
        bool result = false;
        if(data == null)
            return result;
        if(!data.IsObject)
        {
            return result;
        }
        IDictionary tdictionary = data as IDictionary;
        if(tdictionary == null)
            return result;
        if(tdictionary.Contains(key))
        {
            result = true;
        }
        return result;
    }

}
