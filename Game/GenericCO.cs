﻿

public abstract class GenericCO : CombatModifierEffectAdapter
{
	protected double maxAP = 100;
	protected double currentAP = 0;

/*
	public virtual double getAirWeaponDamageModifier()
	{
		return 1.0;
	}  

	public virtual double getGroundWeaponDamageModifier()
	{
		return 1.0;
	} 

	public virtual double getSeaWeaponDamageModifier()
	{
		return 1.0;
	} 

	public virtual double getAirWeaponDefenseModifier()
	{
		return 1.0;
	}  

	public virtual double getGroundWeaponDefenseModifier()
	{
		return 1.0;
	} 

	public virtual double getSeaWeaponDefenseModifier()
	{
		return 1.0;
	} 
*/
	public virtual double getMaxAbilityPoints()
	{
		return maxAP;
	}

	public virtual double getCurrentAbilityPoints()
	{
		return currentAP;
	}

	public virtual void addAbilityPoints(double ap)
	{
		currentAP += ap;
		if (currentAP > maxAP)
		{
			currentAP = maxAP;
		}
	}
	
	public virtual bool canPayAbility()
	{
		return getCurrentAbilityPoints() >= getAbilityCost();
	}

	public virtual bool canPaySuperAbility()
	{
		return getCurrentAbilityPoints() >= getSuperAbilityCost();
	}


	public abstract double getAbilityCost();
	public abstract double getSuperAbilityCost();

	public abstract void doAbility(Player owner);
	public abstract void doSuperAbility(Player owner);
}
