﻿using System;
using System.Collections.Generic;


public sealed class DamageType
{
	public static readonly DamageType LIGHT = new DamageType("LIGHT", InnerEnum.LIGHT, "LIGHT", 0);
	public static readonly DamageType MEDIUM = new DamageType("MEDIUM", InnerEnum.MEDIUM, "MEDIUM", 1);
	public static readonly DamageType HEAVY = new DamageType("HEAVY", InnerEnum.HEAVY, "HEAVY", 2);
	public static readonly DamageType EXPLOSIVE = new DamageType("EXPLOSIVE", InnerEnum.EXPLOSIVE, "EXPLOSIVE", 3);

	private static readonly IList<DamageType> valueList = new List<DamageType>();

	static DamageType()
	{
		valueList.Add(LIGHT);
		valueList.Add(MEDIUM);
		valueList.Add(HEAVY);
		valueList.Add(EXPLOSIVE);
	}

	public enum InnerEnum
	{
		LIGHT,
		MEDIUM,
		HEAVY,
		EXPLOSIVE
	}

	private readonly string nameValue;
	private readonly int ordinalValue;
	private readonly InnerEnum innerEnumValue;
	private static int nextOrdinal = 0;

	internal string name;
	internal int num;
	private static double[,] multiplier = new double[,]
	{
		{1.2, 1.0, 0.6, 1.0},
		{1.2, 1.0, 0.8, 1.0},
		{0.8, 1.5, 1.2, 1.0},
		{1.0, 1.0, 1.5, 1.0}
	};

	//First == Attacker, Second == Defender

	public string Name
	{
		get
		{
			return name;

		}
	}

	private DamageType(string name, InnerEnum innerEnum, string nameVal, int num)
	{
		this.name = name;
		this.num = num;

		nameValue = nameVal;
		ordinalValue = nextOrdinal++;
		innerEnumValue = innerEnum;
	}

	public static DamageType typeFromString(string @string)
	{
		foreach (DamageType type in values())
		{
			if (type.Name.Equals(@string))
			{
				return type;
			}
		}
		MyLogger.log("ERROR NOT FOUND INSIDE DAMAGE TYPE");
		return null;
	}

	public static double getDamageMultiplier(DamageType attack, DamageType defense)
	{
		double d = multiplier[attack.num,defense.num];
		MyLogger.log("Attacker: " + attack.Name + " Defender: " + defense.Name + " factor:" + d);
		return d;
	}

	public static IList<DamageType> values()
	{
		return valueList;
	}

	public InnerEnum InnerEnumValue()
	{
		return innerEnumValue;
	}

	public int ordinal()
	{
		return ordinalValue;
	}

	public override string ToString()
	{
		return nameValue;
	}

	public static DamageType valueOf(string name)
	{
		foreach (DamageType enumInstance in DamageType.values())
		{
			if (enumInstance.nameValue == name)
			{
				return enumInstance;
			}
		}
		throw new System.ArgumentException(name);
	}
}
