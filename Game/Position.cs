﻿using System;
using System.Collections.Generic;


public class Position
{
	public int col;
	public int row;
	public int movesLeft = 0;
	public Position parent;

	public Position()
	{

	}

	public Position(int row, int col)
	{
		this.col = col;
		this.row = row;
	}

	public Position(int row, int col, int movesLeft)
	{
		this.col = col;
		this.row = row;
		this.movesLeft = movesLeft;
	}

	public override bool Equals(object obj)
	{
		if (obj is Position)
		{
			Position pos = (Position) obj;
			return pos.col == this.col && pos.row == this.row;
		}

		return base.Equals(obj);
	}

	public override int GetHashCode()
	{
		int hash = 2 + row / 3 + col * 23 / 7;
		return hash;
	}

	public override string ToString()
	{
		return "Row: " + row + " Col: " + col;
	}

	public static int getDistance(Position unitPos, Position targetPos)
	{
		int distance = 0;

		distance += Math.Abs(unitPos.col - targetPos.col);
		distance += Math.Abs(unitPos.row - targetPos.row);
		return distance;
	}

	// if (radius == 0)
	public static List<Position> getAllPositionsXSquaresAway(Position origin, int radius)
	{
		if (radius == 0)
		{
			return new List<Position>();
		}
		List<Position> pos = new List<Position>();
		for (int row = origin.row - radius; row <= origin.row + radius; row++)
		{
			for (int col = origin.col - radius; col <= origin.col + radius; col++)
			{
				// Off board
				if (row < 0 || row >= Constants.height || col < 0 || col >= Constants.width)
				{
					continue;
				}
				pos.Add(new Position(row, col));
			}
		}
		return pos;
	}

	/// <summary>
	/// returns all positions inside two ranges does not remove origin.
	/// </summary>
	/// <param name="origin"> </param>
	/// <param name="min"> </param>
	/// <param name="max">
	/// @return </param>
	public static List<Position> getAllPositionsUpTo(Position origin, int min, int max)
	{

		List<Position> pos = new List<Position>();
		for (int row = origin.row - max; row <= origin.row + max; row++)
		{
			for (int col = origin.col - max; col <= origin.col + max; col++)
			{
				// Off board
				if (row < 0 || row >= Constants.height || col < 0 || col >= Constants.width)
				{
					continue;
				}
				Position des = new Position(row, col);
				int distance = Position.getDistance(origin, des);
				if (distance >= min && distance <= max)
				{
					pos.Add(des);
				}
			}
		}
		// pos.remove(origin);
		return pos;
	}

	public virtual List<Position> Neighbours
	{
		get
		{

			List<Position> pos = new List<Position>();
			if (col + 1 < Constants.width)
			{
				pos.Add(new Position(row, col + 1));
			}
			// Left
			if (col - 1 >= 0)
			{
				pos.Add(new Position(row, col - 1));
			}
			// UP

			if (row + 1 < Constants.height)
			{
				pos.Add(new Position(row + 1, col));
			}
			// DOWN
			if (row - 1 >= 0)
			{
				pos.Add(new Position(row - 1, col));
			}
			return pos;

		}
	}

	public enum Direction
	{
		LEFT,
		RIGHT,
		DOWN,
		UP,
		ERROR
	}
	public static Direction getDirection(Position from, Position to)
	{
		if (from.row != to.row)
		{
			if (from.row + 1 == to.row)
			{
				return Direction.UP;
			}
			else if (from.row - 1 == to.row)
			{
				return Direction.DOWN;
			}
		}
		else if (from.col != to.col)
		{
			if (from.col + 1 == to.col)
			{
				return Direction.RIGHT;
			}
			else if (from.col - 1 == to.col)
			{
				return Direction.LEFT;
			}
		}
		return Direction.ERROR;
	}
}
