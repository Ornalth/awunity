using System.Collections.Generic;

public sealed class MovementType
{
	public static readonly MovementType AIR = new MovementType("AIR", InnerEnum.AIR);
	public static readonly MovementType SEA = new MovementType("SEA", InnerEnum.SEA);
	public static readonly MovementType TIRE = new MovementType("TIRE", InnerEnum.TIRE);
	public static readonly MovementType FOOT = new MovementType("FOOT", InnerEnum.FOOT);
	public static readonly MovementType TREADS = new MovementType("TREADS", InnerEnum.TREADS);

	private static readonly IList<MovementType> valueList = new List<MovementType>();

	static MovementType()
	{
		valueList.Add(AIR);
		valueList.Add(SEA);
		valueList.Add(TIRE);
		valueList.Add(FOOT);
		valueList.Add(TREADS);
	}

	public enum InnerEnum
	{
		AIR,
		SEA,
		TIRE,
		FOOT,
		TREADS
	}

	private readonly string nameValue;
	private readonly int ordinalValue;
	private readonly InnerEnum innerEnumValue;
	private static int nextOrdinal = 0;

	private string name;
	public MovementType(string name, InnerEnum innerEnum)
	{
		this.name = name;

		nameValue = name;
		ordinalValue = nextOrdinal++;
		innerEnumValue = innerEnum;
	}

	public static MovementType movementTypeFromString(string str)
	{
		foreach (MovementType type in values())
		{
			if (type.name.Equals(str))
			{
				return type;
			}
		}
		return null;
	}



	public static IList<MovementType> values()
	{
		return valueList;
	}

	public InnerEnum InnerEnumValue()
	{
		return innerEnumValue;
	}

	public int ordinal()
	{
		return ordinalValue;
	}

	public override string ToString()
	{
		return nameValue;
	}

	public static MovementType valueOf(string name)
	{
		foreach (MovementType enumInstance in MovementType.values())
		{
			if (enumInstance.nameValue == name)
			{
				return enumInstance;
			}
		}
		throw new System.ArgumentException(name);
	}
}