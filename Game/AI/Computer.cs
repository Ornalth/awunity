﻿using System;




//http://www.checkmarkgames.com/2012/03/building-strategy-game-ai.html
//http://gameschoolgems.blogspot.ca/2009/12/influence-maps-i.html

public abstract class Computer : Player
{



	internal Random rand = new Random();
	protected internal Strategy mode = Strategy.AGGRESSIVE;

	protected internal enum Strategy
	{
		AGGRESSIVE,
		DEFENSIVE,
		BALANCED
	}
	
	public Computer(int team, GenericCO co) : base(team, co)
	{
	}

	public abstract void makeMove();



}
