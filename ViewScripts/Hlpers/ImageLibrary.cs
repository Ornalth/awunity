﻿using UnityEngine;
using System.Collections.Generic;

public class ImageLibrary {


	public static readonly string TILE_LOC = "Images/Tiles/";
	public static readonly string BASE_IMAGE_LOC = "Images/Units/";
	public static readonly string BASE_PROMO_LOC = "Images/Promo/";
	public static readonly List<string> imageLocations = new List<string>(new string[] { BASE_IMAGE_LOC, TILE_LOC });

	static Dictionary <string, Sprite> library = new Dictionary<string,Sprite>();
	private static readonly string ERROR_SPRITE = "ERROR";


	public static Sprite getPromoImage(Unit unit)
	{
		if (unit == null)
		{
			return Resources.Load <Sprite> (BASE_PROMO_LOC + "DEFAULT");
		}
		Sprite spr = Resources.Load <Sprite> (BASE_PROMO_LOC + unit.getRank().getName());
		return spr;
	}
	public static Sprite getImage(string drawableName)
	{
		Sprite ans;
		if (library.TryGetValue(drawableName, out ans))
		{
			return ans;
		}
		else
		{
			foreach (string loc in imageLocations)
			{
				Sprite spr = Resources.Load <Sprite> (loc + drawableName);
				if (spr != null)
				{
					library[drawableName] = spr;
					return spr;
				}
			}
			Debug.Log("THERE IS NO " + drawableName);
			return getErrorSprite();
		}
	}

	public static Sprite getErrorSprite()
	{
		Sprite ans;
		if (!library.TryGetValue(ERROR_SPRITE, out ans))
		{
			ans = Resources.Load <Sprite> (BASE_IMAGE_LOC + ERROR_SPRITE);
			library.Add (ERROR_SPRITE, ans);
		}
		return ans;
	}

	public static void reset()
	{
		library = new Dictionary<string, Sprite>();
	}
}
