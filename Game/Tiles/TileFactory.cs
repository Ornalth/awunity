﻿using System;


public class TileFactory
{

	public static Tile tileFromChar(char c, int row, int col)
	{
		if (c == 'G')
		{
			return new Grass(row, col);
		}
		else if (c == 'S')
		{

			return new Sea(row, col);
		}

		else if (c == 'F')
		{
			return new Forest(row, col);
		}
		else if (c == 'M')
		{
			return new Mountain(row, col);
		}
		else if (c == 'R')
		{
			return new Road(row, col);
		}

		else if (c == 'h')
		{
			return new Headquarters(row, col);
		}
		else if (c == 'p')
		{
			return new Seaport(row, col);
		}
		else if (c == 'a')
		{
			return new Airport(row, col);
		}
		else if (c == 'c')
		{
			return new City(row, col);
		}
		else if (c == 'b')
		{
			return new Barracks(row, col);
		}

		MyLogger.log("ERROR in tilefactory!");
		return null;
	}

	public static string getDrawableName(Tile tile)
	{
		if (tile is Grass)
		{
			return "grass";
		}
		else if (tile is Forest)
		{
			return "forest";
		}

		else if (tile is Mountain)
		{
			return "mountain";
		}
		else if (tile is Road)
		{
			return "road";
		}
		else if (tile is Sea)
		{
			return "sea";
		}
		//Buildings
		else if (tile is City)
		{
			return "city" + ((Building) tile).getTeam();
		}
		else if (tile is Airport)
		{
			return "airport" + ((Building) tile).getTeam();
		}
		else if (tile is Barracks)
		{
			return "barracks" + ((Building) tile).getTeam();
		}
		else if (tile is Headquarters)
		{
			return "hq" + ((Building) tile).getTeam();
		}
		else if (tile is Seaport)
		{
			return "seaport" + ((Building) tile).getTeam();
		}
		MyLogger.log("ERROR DID NOT FIND THIS?");
		return null;
	}

}