﻿
public class Headquarters : Building
{
	protected internal Headquarters(int row, int col) : base(row, col)
	{
	}
	public override string ToString()
	{
		return "BH" + team;
	}
	
	public override int getDefense()
	{
		return 4;
	}
}
