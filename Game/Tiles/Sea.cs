﻿
public class Sea : Tile
{

	protected internal Sea(int row, int col) : base(row, col)
	{
	}

	public override string ToString()
	{
		return "SEA";
	}

	protected internal override int getFootUnitCost()
	{
		return 9999;
	}

	protected internal override int getTireUnitCost()
	{
		return 9999;
	}

	protected internal override int getTreadUnitCost()
	{
		return 9999;
	}

	protected internal override int getSeaUnitCost()
	{
		return 1;
	}

	protected internal override int getAirUnitCost()
	{
		return 1;
	}
	
	public override int getDefense()
	{
		return 0;
	}
}

