﻿using System.Collections.Generic;


public abstract class Building : Tile
{


	internal int team;
	internal int health;

	protected internal Building(int row, int col) : base(row, col)
	{
		this.health = Constants.BUILDING_MAX_HEALTH;
	}

	public override bool isBuilding()
	{
		return true;
	}

	public int getTeam()
	{
		return team;
	}

	public void setTeam(int team)
	{
		this.team = team;
	}




	public virtual void restoreHealth()
	{
		health += Constants.BUILDING_HEALTH_REGEN;
		if (health > Constants.BUILDING_MAX_HEALTH)
		{
			health = Constants.BUILDING_MAX_HEALTH;
		}
	}

	public virtual int getHealth()
	{
		return health;
	}

	protected internal override int getFootUnitCost()
	{
		return 1;
	}

	protected internal override int getTireUnitCost()
	{
		return 1;
	}

	protected internal override int getTreadUnitCost()
	{
		return 1;
	}

	protected internal override int getSeaUnitCost()
	{
		return 9999;
	}

	protected internal override int getAirUnitCost()
	{
		return 1;
	}

	public override int getDefense()
	{
		return 3;
	}

	public List<Unit> getGenerateOptions()
	{
		return UnitFactory.generateList(this);
	}

	public virtual bool hasGenerateAbility()
	{
		return UnitFactory.canGenerate(this);
	}

	public virtual bool canGenerate()
	{
		return UnitFactory.canGenerate(this) && this.unit == null;
	}

	public virtual bool cap()
	{

		health = (int)(health - unit.getCapSpeed() * unit.getHealth());
		if (health <= 0)
		{
			health = Constants.BUILDING_MAX_HEALTH;
			//this.getTeam() = getUnit().getTeam();
			return true;
		}
		return false;

	}


}
