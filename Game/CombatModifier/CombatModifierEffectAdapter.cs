﻿
public class CombatModifierEffectAdapter : ICombatModifierEffect
{
	public virtual double getAirWeaponDamageModifier()
	{
		return 1.0;
	}  

	public virtual double getGroundWeaponDamageModifier()
	{
		return 1.0;
	} 
	

	public virtual double getSeaWeaponDamageModifier()
	{
		return 1.0;
	} 

	public virtual double getAirWeaponDefenseModifier()
	{
		return 1.0;
	}  

	public virtual double getGroundWeaponDefenseModifier()
	{
		return 1.0;
	} 

	public virtual double getSeaWeaponDefenseModifier()
	{
		return 1.0;
	} 
}