using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public static class ViewHelper {

	private static float EPSILION = 0.0001f;
	public static readonly string NO_COST_STR= "Zero";
	public static readonly float pixelsToUnits = 64f;
	static Dictionary<string, string> emoticonDictionary;

	public static int GRAVE_CARD_Z_VALUE = -2;

	public static int BIND_DIR_TOP = 1 << 1;
	public static int BIND_DIR_BOTTOM =  1 << 2;
	public static int BIND_DIR_RIGHT = 1 << 3;
	public static int BIND_DIR_LEFT =  1 << 4;
	public static int BIND_CENTER_VERTICAL = 1 << 5; // TODO
	public static int BIND_CENTER_HORIZONTAL = 1 << 6; //TODO




	public static float getGenericCardWidth()
	{
		return 0.8f;
	}

	public static float getZoomCardWidth()
	{
		return 1.8f;
	}


	public static void resolveTextSizeContinue(TextMesh mesh, string input, float size)
	{
		string builder = "";
		mesh.text = "";
		float rowLimit = size;//SizeHelper.getGenericCardWidth ();
		string text = input;
		string[] parts = text.Split(' ');
		for (int i = 0; i < parts.Length; i++)
		{
			//MyLogger.log(parts[i]);
			mesh.text += parts[i] + " ";
			if (mesh.GetComponent<Renderer>().bounds.extents.x > rowLimit)
			{
				mesh.text = builder.TrimEnd() + System.Environment.NewLine + parts[i] + " ";
			}
			builder = mesh.text;
		}
	}
	
	public static void resolveTextSizeEllipses(TextMesh mesh, string input, float size)
	{
		string builder = "";
		mesh.text = "";
		float rowLimit = size;//0.9f; //find the sweet spot
		string text = input;
		string[] parts = text.Split(' ');
		for (int i = 0; i < parts.Length; i++)
		{
			//MyLogger.log(parts[i]);
			mesh.text += parts[i] + " ";
			if (mesh.GetComponent<Renderer>().bounds.extents.x > rowLimit)
			{
				mesh.text = builder.TrimEnd() + "...";
			}
			builder = mesh.text;
		}
	}

	public static void hide(GameObject view)
	{
		view.SetActive (false);

	}
	
	public static void show(GameObject view)
	{
		view.SetActive (true);
	}

	public static bool parseInt(string text, out int a)
	{
		if(!int.TryParse(text, out a))
		{
			a = 0;
			return false;
		}
		return true;
	}

	public static Dictionary<string,string> getEmoticonDictionary()
	{
		if (emoticonDictionary == null)
		{
			emoticonDictionary = new Dictionary<string,string>();
			emoticonDictionary["redMana"] = ":redMana:";
			emoticonDictionary["blueMana"] = ":blueMana:";
			emoticonDictionary["greenMana"] = ":greenMana:";
			emoticonDictionary["grayMana"] = ":grayMana:";
			emoticonDictionary["dizzy"] = ":dizzy:"; //
			emoticonDictionary["dizzyLock"] = ":dizzyLock:";
			emoticonDictionary["speedS"] = ":S:";
			emoticonDictionary["speedA"] = ":A:";
			emoticonDictionary["speedB"] = ":B:";
			emoticonDictionary["speedC"] = ":C:";
			emoticonDictionary["speedD"] = ":D:";
			
		}

		return emoticonDictionary;
	}

/*
	public static GameObject addChild(GameObject parent, GameObject prefab, GameObject eventReceiver)
	{
		GameObject obj = NGUITools.AddChild (parent, prefab);

		UICheckbox[] checkboxes = obj.GetComponentsInChildren<UICheckbox>();
		foreach (UICheckbox check in checkboxes)
		{
			check.eventReceiver = eventReceiver;
		}

		UIInput[] inputs = obj.GetComponentsInChildren<UIInput> ();
		foreach (UIInput input in inputs)
		{
			input.eventReceiver = eventReceiver;
		}

		UIPopupList[] lists = obj.GetComponentsInChildren<UIPopupList>();
		foreach (UIPopupList list in lists)
		{
			list.eventReceiver = eventReceiver;
		}
		return obj;
	}
*/
	public static Texture2D convertSpriteToTexture(Sprite sprite)
	{
		Texture2D croppedTexture = new Texture2D( (int)sprite.rect.width, (int)sprite.rect.height );
 
		Color[] pixels = sprite.texture.GetPixels( (int)sprite.textureRect.x,
		(int)sprite.textureRect.y,
		(int)sprite.textureRect.width,
		(int)sprite.textureRect.height );
		 
		croppedTexture.SetPixels( pixels );
		croppedTexture.Apply();

		return croppedTexture;
 
	}

	public static bool isColorEqual(Color c1, Color c2)
	{
		if((int)(c1.r * 1000) == (int)(c2.r * 1000) &&
			(int)(c1.g * 1000) == (int)(c2.g * 1000) &&
			(int)(c1.b * 1000) == (int)(c2.b * 1000))
		{
			return true;
		}
		return false;
	}

	public static bool V3Equals(Vector3 v1, Vector3 v2)
	{
		return Vector3.SqrMagnitude(v1 - v2) < EPSILION;
	}

	public static void resizeSpriteToScreen(GameObject theSprite, Camera theCamera, float fitToScreenWidth, float fitToScreenHeight)
	{
		SpriteRenderer sr = theSprite.GetComponent<SpriteRenderer>();
		theSprite.transform.localScale = new Vector3(1,1,1);
		float width = sr.sprite.bounds.size.x;
		float height = sr.sprite.bounds.size.y;
		float worldScreenHeight = (float)(theCamera.orthographicSize * 2.0);
		float worldScreenWidth = (float)(worldScreenHeight / Screen.height * Screen.width);
		if (fitToScreenWidth != 0)
		{
			fitToScreenWidth = 1.0f/fitToScreenWidth;
			Vector2 sizeX = new Vector2(worldScreenWidth / width / fitToScreenWidth,theSprite.transform.localScale.y);
			theSprite.transform.localScale = sizeX;
		}

		if (fitToScreenHeight != 0)
		{
			fitToScreenHeight = 1.0f/fitToScreenHeight;
			Vector2 sizeY = new Vector2(theSprite.transform.localScale.x, worldScreenHeight / height / fitToScreenHeight);
			theSprite.transform.localScale = sizeY;
		}
	}
	
	public static void bindLeft(GameObject theSprite, Camera theCamera)
	{
		SpriteRenderer sr = theSprite.GetComponent<SpriteRenderer>();
		float width = sr.bounds.size.x;
		float worldScreenHeight = (float)(theCamera.orthographicSize * 2.0);
		float worldScreenWidth = (float)(worldScreenHeight / Screen.height * Screen.width);
		float scaleX = (worldScreenWidth - width) / 2;
		theSprite.transform.localPosition = new Vector3(-scaleX, theSprite.transform.localPosition.y, theSprite.transform.localPosition.z);
	}

	public static void bindBottom(GameObject theSprite, Camera theCamera)
	{
		SpriteRenderer sr = theSprite.GetComponent<SpriteRenderer>();
		float height = sr.bounds.size.y;
		float worldScreenHeight = (float)(theCamera.orthographicSize * 2.0);
		//float worldScreenWidth = (float)(worldScreenHeight / Screen.height * Screen.width);
		
		//width = sr.bounds.size.x;
		float scaleY = (worldScreenHeight - height) / 2;
		//MyLogger.log("ORGO" + theCamera.orthographicSize + " " + width + " " + worldScreenWidth);
		theSprite.transform.localPosition = new Vector3(theSprite.transform.localPosition.x,-scaleY, theSprite.transform.localPosition.z);
	}

	public static void bindRight(GameObject theSprite, Camera theCamera)
	{
		bindLeft(theSprite, theCamera);
		Vector3 v3 = theSprite.transform.localPosition;
		v3.x *= -1;
		theSprite.transform.localPosition = v3;
	}

	public static void bindTop(GameObject theSprite, Camera theCamera)
	{
		bindBottom(theSprite, theCamera);
		Vector3 v3 = theSprite.transform.localPosition;
		v3.y *= -1;
		theSprite.transform.localPosition = v3;
	}

	public static void bind(GameObject theSprite, Camera theCamera, int dir)
	{
		if (HelperUtils.testBits(dir, BIND_DIR_TOP))
		{
			MyLogger.log("BIND TOP");
			bindTop(theSprite, theCamera);
		}
		if (HelperUtils.testBits(dir, BIND_DIR_BOTTOM))
		{
			MyLogger.log("BIND BOTTOM");
			bindBottom(theSprite, theCamera);
		}
		if (HelperUtils.testBits(dir, BIND_DIR_LEFT))
		{
			MyLogger.log("BIND LEFT");
			bindLeft(theSprite, theCamera);
		}
		if (HelperUtils.testBits(dir, BIND_DIR_RIGHT))
		{
			MyLogger.log("BIND RIGHT");
			bindRight(theSprite, theCamera);
		}
	}

	public static void resizePixelHeight (GameObject theSprite, Camera theCamera, float height)
	{
		SpriteRenderer sr = theSprite.GetComponent<SpriteRenderer>();
		theSprite.transform.localScale = new Vector3(theSprite.transform.localScale.x,1,theSprite.transform.localScale.z);
		//float width = sr.bounds.size.x;
		float prevHeight = sr.bounds.size.y;
		float scaleY = height/prevHeight;
		theSprite.transform.localScale = new Vector3(theSprite.transform.localScale.x,scaleY,theSprite.transform.localScale.z);

	}

	public static void resizePixelWidth (GameObject theSprite, Camera theCamera, float width)
	{
		
	}

	

	//does by upperleft corner
	public static Vector3 getVectorScaleFor (GameObject theObj, GameObject theParent, float x, float y)
	{
		SpriteRenderer sr = theParent.GetComponent<SpriteRenderer>();
		float height = sr.bounds.size.y;
		float scaleY = (float)((height/2.0f-y)/height);
	
		MyLogger.log("HEIGHT IS " + height + " Y is " + y + " SCALE Y " + scaleY );

		float width = sr.bounds.size.x;
		float scaleX = (float)((width/2.0f-x)/width);
				MyLogger.log("width IS " + width + " X is " + x + " SCALE X " + scaleX);

		return new Vector3(scaleX,scaleY,0);
	}

	public static int getScreenWidth()
	{
		return Screen.width;
	}

	public static int getScreenHeight()
	{
		return Screen.height;
	}

	public static void setTextColor(TypogenicText text, Color c)
	{
		text.ColorTopLeft = c;
		text.ColorTopRight = c;
		text.ColorBottomLeft = c;
		text.ColorBottomRight = c;
	}
}
