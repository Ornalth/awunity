﻿using System;
using System.Collections.Generic;

public sealed class UnitRank
{
	public static readonly UnitRank RECRUIT = new UnitRank("RECRUIT", InnerEnum.RECRUIT, 0);
	public static readonly UnitRank SERGEANT = new UnitRank("SERGEANT", InnerEnum.SERGEANT, 2);
	public static readonly UnitRank LIEUTENANT = new UnitRank("LIEUTENANT", InnerEnum.LIEUTENANT, 5);

	private static readonly IList<UnitRank> valueList = new List<UnitRank>();

	static UnitRank()
	{
		valueList.Add(RECRUIT);
		valueList.Add(SERGEANT);
		valueList.Add(LIEUTENANT);
	}

	public enum InnerEnum
	{
		RECRUIT,
		SERGEANT,
		LIEUTENANT
	}

	private readonly string nameValue;
	private readonly int ordinalValue;
	private readonly InnerEnum innerEnumValue;
	private static int nextOrdinal = 0;
	private string name;
	private int numKillsRequired;

	private UnitRank(string name, InnerEnum innerEnum, int numKillsReq)
	{
		this.name = name;
		numKillsRequired = numKillsReq;

		nameValue = name;
		ordinalValue = nextOrdinal++;
		innerEnumValue = innerEnum;
	}

	public static UnitRank getRank(int numKills)
	{
		UnitRank highestRank = RECRUIT;
		foreach (UnitRank rank in values())
		{
			if (numKills >= rank.numKillsRequired)
			{
				highestRank = rank;
			}
		}
		return highestRank;
	}

	public static UnitRank rankFromString(string @string)
	{
		foreach (UnitRank rank in values())
		{
			if (rank.getName().Equals(@string))
			{
				return rank;
			}
		}
		return null;
	}


	public string getName()
	{
		return name;
	}

	public static IList<UnitRank> values()
	{
		return valueList;
	}

	public InnerEnum InnerEnumValue()
	{
		return innerEnumValue;
	}

	public int ordinal()
	{
		return ordinalValue;
	}

	public override string ToString()
	{
		return nameValue;
	}

	public static UnitRank valueOf(string name)
	{
		foreach (UnitRank enumInstance in UnitRank.values())
		{
			if (enumInstance.nameValue == name)
			{
				return enumInstance;
			}
		}
		throw new System.ArgumentException(name);
	}
}