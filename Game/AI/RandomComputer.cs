﻿using System.Collections.Generic;



public class RandomComputer : Computer
{

	public RandomComputer(int team, GenericCO co) : base(team, co)
	{
	}

	public override void makeMove()
	{
		// Currently random;
		Board board = Board.getInstance();

		List<Unit> units = board.getUnitsForTeam(team);
		//int[][] influenceMap = calcInfluence();
		foreach (Unit unit in units)
		{
			// Move to random location
			Tile origin = board.getTile(unit);
			board.cap(origin);

			if (unit.canMove())
			{
				List<Position> moves = board.getPossibleMoves(board.getTile(unit));
				if (moves.Count > 0)
				{
					Position p = moves[rand.Next(moves.Count)];
					// TODO logic for a specific move...

					board.move(origin, board.getTile(p));
				}
			}

			// Attack
			if (unit.canAttack())
			{
				// Check with prim
				bool hasAttacked = false;
				if (unit.getPrimaryWeapon().hasAmmo())
				{
					List<Position> targets = board.getTargets(origin, true);
					if (targets.Count > 0)
					{
						Position p = targets[rand.Next(targets.Count)];
						board.attack(origin, board.getTile(p), true);
						hasAttacked = true;
					}
				}

				// 2ndary
				if (!hasAttacked && unit.getSecondaryWeapon().hasAmmo())
				{
					List<Position> targets = board.getTargets(origin, false);
					if (targets.Count > 0)
					{
						Position p = targets[rand.Next(targets.Count)];
						board.attack(origin, board.getTile(p), false);
						hasAttacked = true;
					}
				}

			}

		}

		List<Building> buildings = board.getBuildingsForTeam(team);

		foreach (Building build in buildings)
		{
			if (build.canGenerate())
			{
				List<Unit> canGen = build.getGenerateOptions();
				foreach (Unit unit in canGen)
				{
					if (UnitFactory.getCost(unit) < gold)
					{
						board.purchase(unit, build);
						break;
					}
				}
			}
		}

		board.endTurn();
	}

}

