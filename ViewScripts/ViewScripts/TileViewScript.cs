using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class TileViewScript : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler {

	public interface TileObserver
	{
		void tileClicked(TileViewScript scr);
		void tileEntered(TileViewScript scr);
	}
	
	public enum State
	{
		MOVE_TARGET, ATTACK_TARGET, SELECTED, NONE, BUILD, CONFIRM_ATTACK_TARGET
	}

	IDrawable drawableTile;
	IDrawable drawableUnit;
	SpriteRenderer unitRenderer;
	SpriteRenderer tileRenderer;
	SpriteRenderer highlightRenderer;
	List<TileObserver> observers;
	TypogenicText unitHealthText;
	SpriteRenderer buildingHealthRenderer;
	SpriteRenderer buildingMaxHealthRenderer;
	SpriteRenderer rankRenderer;
	Position pos;

	State state = State.NONE;
	bool isFinished = false;
	// Use this for initialization
	void Awake () {
		observers = new List<TileObserver>();
		SpriteRenderer[] sprites = gameObject.GetComponentsInChildren<SpriteRenderer>();

		foreach (SpriteRenderer renderer in sprites)
		{
			if (renderer.name.Equals("Unit"))
			{
				unitRenderer = renderer;
			}
			else if (renderer.name.Equals("UnitRank"))
			{
				rankRenderer = renderer;
			}	
			else if (renderer.name.Equals("Building"))
			{
				tileRenderer = renderer;
			}
			else if (renderer.name.Equals("Highlight"))
			{
				highlightRenderer = renderer;
				Color color = highlightRenderer.color;
				color.a = 0.2f;
				highlightRenderer.color = color;
			}
			else if (renderer.name.Equals("BuildingHealthLeft"))
			{
				buildingHealthRenderer = renderer;
				
			}
			else if (renderer.name.Equals("BuildingHealthMax"))
			{
				buildingMaxHealthRenderer = renderer;
				
			}
		}

		TypogenicText[] texts = gameObject.GetComponentsInChildren<TypogenicText>();

		foreach (TypogenicText text in texts)
		{
			if (text.name.Equals("UnitHealth"))
			{
				unitHealthText = text;
			}	
		}


		BoxCollider2D collider = GetComponent<BoxCollider2D>();
		collider.size = GetComponent<SpriteRenderer>().bounds.size;
		clearHighlights();
	}

	public void setPosition(Position position)
	{
		pos = position;
	}
	public Position getPosition()
	{
		return pos;
	}
	public void setTile(Tile t)
	{
		setTileDrawable(t);
		setUnitDrawable(t.getUnit());

		updateUnitHealth(t.getUnit());
		updateUnitRank(t.getUnit());
		updateBuildingHealth(t);
	}

	private void updateUnitRank(Unit unit)
	{
		rankRenderer.sprite = ImageLibrary.getPromoImage(unit);
	}

	public void updateBuildingHealth(Tile t)
	{
		if (t.isBuilding())
		{
			Building building = (Building) t;
			int health = building.getHealth();
			
			if (health < Constants.BUILDING_MAX_HEALTH)
			{
				float percentHealth = (float)health/ Constants.BUILDING_MAX_HEALTH;
				showBuildingHealth(percentHealth);
				//Change alpha
			//	buildingHealthRenderer.

			}
			else
			{
				hideBuildingHealth();
			}
		}
		else
		{
			hideBuildingHealth();
		}

	}

	private void showBuildingHealth(float percentHealth)
	{
		Vector3 size = buildingHealthRenderer.transform.localScale;
		size.y = percentHealth;
		buildingHealthRenderer.transform.localScale = size;

		Color color = buildingHealthRenderer.color;
		color.a = 1.0f;
		buildingHealthRenderer.color = color;

		color = buildingMaxHealthRenderer.color;
		color.a = 1.0f;
		buildingMaxHealthRenderer.color = color;
	}

	private void hideBuildingHealth()
	{
		Color color = buildingHealthRenderer.color;
		color.a = 0.0f;
		buildingHealthRenderer.color = color;

		color = buildingMaxHealthRenderer.color;
		color.a = 0.0f;
		buildingMaxHealthRenderer.color = color;
	}

	public void updateUnitHealth(Unit unit)
	{
		if (unit == null)
		{
			unitHealthText.Text = "";
		}
		else
		{
			unitHealthText.Text = "" + unit.getHealth();
		}
	}

	public void setUnitDrawable(IDrawable drawable)
	{
		if (drawable == null)
		{
			unitRenderer.enabled = false;
			drawableUnit = null;
		}
		else 
		{
			drawableUnit = drawable;
			unitRenderer.sprite = ImageLibrary.getImage(drawableUnit.getDrawableName());
			unitRenderer.enabled = true;
		}
		
	}

	public void setTileDrawable(IDrawable drawable)
	{
		if (drawable == null)
		{
			tileRenderer.enabled = false;
			drawableTile = null;
		}
		else
		// (drawableTile == null || !drawableTile.getDrawableName().Equals(drawable.getDrawableName()))
		{
			drawableTile = drawable;
			tileRenderer.sprite = ImageLibrary.getImage(drawableTile.getDrawableName());
			tileRenderer.enabled = true;
		}
	}

	public void subscribe(TileObserver obs)
	{
		observers.Add(obs);
	}

	public void OnPointerClick(PointerEventData data)
     {
	    foreach (TileObserver obs in observers)
		{
			obs.tileClicked(this);
		}
     }

    public void OnPointerEnter(PointerEventData data)
     {
	    foreach (TileObserver obs in observers)
		{
			obs.tileEntered(this);
		}
     }

         /*
	public void OnMouseDown()
	{
		foreach (TileObserver obs in observers)
		{
			obs.tileClicked(this);
		}
	}*/

	public void setFinished(bool val)
	{
		isFinished = val;
		clearHighlights();
	}

	public void setHighlight(State state)
	{
		updateSelection(state);
	}
	
	private void updateSelection(State state)
	{
//		Debug.Log("HIGHLIGHT");
		switch (state)
		{
			case State.ATTACK_TARGET:
				// holder.highlight.setColor(Color.RED);
				changeHighlight(Color.red);
				break;
			case State.CONFIRM_ATTACK_TARGET:
				// holder.highlight.setColor(Color.RED);
				changeHighlight(Color.red);
				break;
			case State.MOVE_TARGET:
				// holder.highlight.setColor(Color.YELLOW);
				changeHighlight(Color.yellow);
				break;
			case State.BUILD:
				// holder.highlight.setColor(Color.YELLOW);
				changeHighlight(Color.green);
				break;
			case State.NONE:
				clearHighlights();
				break;
			case State.SELECTED:
				// holder.highlight.setColor(Color.GREEN);
				changeHighlight(Color.green);
				break;
			default:
				break;

		}

	}

	private void clearHighlights()
	{
		Color color = highlightRenderer.color;
		color.a = 0.0f;
		if (isFinished)
		{
			color = Color.gray;
			color.a = 0.55f;
		}
		highlightRenderer.color = color;
	}

	private void changeHighlight(Color color)
	{
		color.a = 0.5f;
		highlightRenderer.color = color;

	}
	
}
