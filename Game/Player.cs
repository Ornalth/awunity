﻿using System;

public class Player : ICombatModifierEffect
{

	internal int gold;
	internal int team;
	internal double costFactor = 1.0;
	GenericCO co;
	
	public Player(int team, GenericCO co)
	{
		this.co = co;
		gold = Constants.STARTING_GOLD;
		this.team = team;
	}

	public virtual void addGenerationGold(int amount)
	{
		gold += amount;
	}

	public int getGold(){
		return gold;
	}

	public virtual bool pay(int cost)
	{
		gold -= cost;
		if (gold < 0)
		{
			MyLogger.log("OMFG ERROR NEGATIVE GOLD.");
			gold += cost;
			return false;
		}
		return true;
	}

	public int getTeam()
	{
		return team;
	}

	public void setTeam(int team)
	{
		this.team = team;
	}


	//CO stuff.
	public double getAirWeaponDamageModifier()
	{
		return co.getAirWeaponDamageModifier();
	}  

	public double getGroundWeaponDamageModifier()
	{
		return co.getGroundWeaponDamageModifier();
	} 

	public double getSeaWeaponDamageModifier()
	{
		return co.getSeaWeaponDamageModifier();
	} 

	public double getAirWeaponDefenseModifier()
	{
		return co.getAirWeaponDefenseModifier();
	}  

	public double getGroundWeaponDefenseModifier()
	{
		return co.getGroundWeaponDefenseModifier();
	} 

	public double getSeaWeaponDefenseModifier()
	{
		return co.getSeaWeaponDefenseModifier();
	} 

	public void addAbilityPoints(double ap)
	{
		co.addAbilityPoints(ap);
	}

	public double getCurrentAbilityPoints()
	{
		return co.getCurrentAbilityPoints();
	}

	public double getMaxAbilityPoints()
	{
		return co.getMaxAbilityPoints();
	}

	public void activateCOAbility()
	{
		co.doAbility(this);
	}

	public void activateSuperCOAbility()
	{
		co.doSuperAbility(this);
	}

	public bool canPayAbility()
	{
		return co.canPayAbility();
	}

	public bool canPaySuperAbility()
	{
		return co.canPaySuperAbility();
	}


}
