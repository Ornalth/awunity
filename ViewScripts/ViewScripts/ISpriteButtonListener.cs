﻿using UnityEngine;
using System.Collections;


public interface ISpriteButtonListener
{
	void buttonClicked(SpriteButtonScript scr);
}

