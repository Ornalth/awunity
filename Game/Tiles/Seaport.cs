﻿
public class Seaport : Building
{
	protected internal Seaport(int row, int col) : base(row, col)
	{
	}

	public override string ToString()
	{
		return "BP" + team;
	}

	protected internal override int getSeaUnitCost()
	{
		return 1;
	}

	public override int getDefense()
	{
		return 3;
	}
}
