﻿
public class Road : Tile
{
	protected internal Road(int row, int col) : base(row, col)
	{
	}

	public override string ToString()
	{
		return "ROA";
	}

	protected internal override int getFootUnitCost()
	{
		return 1;
	}

	protected internal override int getTireUnitCost()
	{
		return 1;
	}

	protected internal override int getTreadUnitCost()
	{
		return 1;
	}

	protected internal override int getSeaUnitCost()
	{
		return 9999;
	}

	protected internal override int getAirUnitCost()
	{
		return 1;
	}

	public override int getDefense()
	{
		return 0;
	}
}
