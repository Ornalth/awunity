﻿public class CombatModifierEffectAbility
{
	int owner;
	int duration;
	bool affectsOwner;
	bool affectsDefender;

	ICombatModifierEffect effect;

	public CombatModifierEffectAbility(int src, int duration, bool affectsOwner, bool affectsDefender, ICombatModifierEffect effect)
	{
		owner = src;
		this.duration = duration;
		this.affectsOwner = affectsOwner;
		this.affectsDefender = affectsDefender;
		this.effect = effect;
	}

	public bool affectsTargetTeam(int target)
	{
		if (owner == target && affectsOwner)
			return true;
		if (owner != target && affectsDefender)
			return true;
		return false;
	}

	public ICombatModifierEffect getEffect()
	{
		return effect;
	}

	public void decreaseDuration()
	{
		duration--;
	}

	public bool isFinished()
	{
		return duration <= 0;
	}
}