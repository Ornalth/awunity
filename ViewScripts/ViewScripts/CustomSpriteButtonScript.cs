﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomSpriteButtonScript : SpriteButtonScript {
	string message;
	public string tag;
	SpriteRenderer backgroundRenderer;
	TypogenicText messageText;
	BoxCollider2D collider;
	void Awake()
	{
		TypogenicText[] texts = GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText text in texts)
		{
			if (text.name.Equals("Message"))
			{
				messageText = text;
			}
			MyLogger.log("WHAT THE MESSAGE" + text.name);
			
		}

		SpriteRenderer[] renders = GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer render in renders)
		{
			if (render.name.Equals("Base"))
			{
				backgroundRenderer = render;
			}
		}

		collider = GetComponent<BoxCollider2D>();
	}

	public void setTag(string s)
	{
		tag = s;
	}

	public string getTag()
	{
		return tag;
	}

	public void setMessage(string m)
	{
		message = m;
		messageText.Text = message; 
		//Resize.
	}

	public void resize(float width, float height)
	{

		Vector3 v3 = backgroundRenderer.transform.localScale;
		v3.x = width / ViewHelper.pixelsToUnits;
		v3.y = height / ViewHelper.pixelsToUnits;
		backgroundRenderer.transform.localScale = v3;
		collider.size = new Vector3(v3.x, v3.y, 1);
	}
}
